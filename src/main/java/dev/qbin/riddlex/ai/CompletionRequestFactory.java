package dev.qbin.riddlex.ai;

import dev.qbin.gaiji.openai.api.model.OpenAiRequest.CompletionRequestBuilder;
import dev.qbin.gaiji.openai.api.model.OpenAiRequest.CompletionRequestBuilder.CompletionRequest;

public class CompletionRequestFactory {
	private CompletionRequestFactory() {
	}

	/**
	 * Designed for solid generation
	 * 
	 * @param model
	 * @param prompt
	 * @return
	 */
	public static CompletionRequest newAbraxas(final String model, final String prompt) {
		return newBuilder(model, prompt).setTemperature(0.7).setTopP(1).setMaxTokens(50).setBest_of(2).build();
	}

	/**
	 * Designed for classification
	 * 
	 * @param model
	 * @param prompt
	 * @param resultCount
	 * @return
	 */
	public static CompletionRequest newBellerophon(final String model, final String prompt, final int resultCount) {
		return newBuilder(model, prompt).setTemperature(1).setTopP(1).setMaxTokens(1).setLogprobs(resultCount).build();
	}

	private static CompletionRequestBuilder newBuilder(final String model, final String prompt) {
		return new CompletionRequestBuilder().setModel(model).setPrompt(prompt);
	}

}
