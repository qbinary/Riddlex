package dev.qbin.riddlex.ai;

import dev.qbin.gaiji.exception.GaijiHttpException;
import dev.qbin.gaiji.openai.api.OpenAiAPI;
import dev.qbin.gaiji.openai.api.model.OpenAiObject.Completion;
import dev.qbin.gaiji.openai.api.model.OpenAiRequest.CompletionRequestBuilder;
import dev.qbin.riddlex.Constants;
import dev.qbin.riddlex.util.LookupProvider;
import dev.qbin.riddlex.util.log.Log;

public class AiInterface implements LookupProvider<String, String> {
	private OpenAiAPI api;

	public AiInterface(final OpenAiAPI api) {
		this.api = api;
	}

	public String createRiddle(final String solution) throws GaijiHttpException {
		CompletionRequestBuilder request = new CompletionRequestBuilder();

		request.setModel("text-davinci-002").setTemperature(0.9).setTopP(1)
				.setPrompt(String.format(Constants.RIDDLE_REQUEST, solution)).setBest_of(2).setMaxTokens(100);

		Completion completion = api.createCompletion(request.build());

		Log.l(solution);

		return completion.choices[0].text.trim().replace("\n", "");
	}

	@Override
	public String provide(String key) {
		try {
			return createRiddle(key);
		} catch (GaijiHttpException e) {
			e.printStackTrace();
		}
		return null;
	}
}
