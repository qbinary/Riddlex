package dev.qbin.riddlex;

public enum Contributor {

	QBIN(ContributorType.CREATOR, "qbin", "Yeah, it's me"),
	IRMBAZIRM(ContributorType.CONCEPT, "Irmbazirm", "Helps with the concepts"),
	SALT_DISPENSER(ContributorType.TRANSLATION, "Salt Dispenser", "Helps with the texts"),
	DALLE(ContributorType.ART, "DALL·E", "Used to create the artworks");

	private ContributorType type;
	private String name;
	private String description;

	public enum ContributorType {
		CREATOR(":gear:"), CODE(":keyboard:"), CONCEPT(":bulb:"), TRANSLATION(":globe_with_meridians:"), TEST(":boom:"),
		ART(":paintbrush:");

		private String emoji;

		ContributorType(String string) {
			this.emoji = string;
		}

		public String getEmoji() {
			return emoji;
		}
	}

	private Contributor(final ContributorType type, final String name, final String description) {
		this.type = type;
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public ContributorType getType() {
		return type;
	}

	public String getDescription() {
		return description;
	}

}
