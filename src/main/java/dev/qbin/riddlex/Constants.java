package dev.qbin.riddlex;

import java.awt.Color;

import dev.qbin.riddlex.bot.feature.command.HelpFeature;
import dev.qbin.riddlex.bot.feature.command.HereFeature;
import dev.qbin.riddlex.bot.feature.command.InformationFeature;
import dev.qbin.riddlex.bot.feature.command.ScoreboardFeature;
import dev.qbin.riddlex.bot.feature.command.SubmitFeature;
import dev.qbin.riddlex.bot.feature.misc.RatingFeature;
import dev.qbin.riddlex.bot.feature.periodic.DailyRiddleFeature;
import dev.qbin.riddlex.bot.ui.ButtonId;
import dev.qbin.riddlex.model.currency.Currency;
import net.dv8tion.jda.api.Permission;

public class Constants {

	public static final String FOOTPRINT = "riddlex_beta_0.3.2";
	public static final boolean DEBUG_BUILD = FOOTPRINT.toLowerCase().contains("alpha");
	public static final boolean RELEASE_BUILD = FOOTPRINT.toLowerCase().contains("release");

	public static final Class<?>[] ACTIVE_FEATURES = new Class<?>[] { HereFeature.class, SubmitFeature.class,
			DailyRiddleFeature.class, InformationFeature.class, ScoreboardFeature.class, RatingFeature.class,
			HelpFeature.class };

	public static final ButtonId[] DEBUG_MENU_BUTTONS = new ButtonId[] { ButtonId.PAY_AND_CREATE_GAME,
			ButtonId.ZERO_TICKETS, ButtonId.ADD_TEN_TICKETS, ButtonId.DEL_THIS, ButtonId.VOID_GAME,
			ButtonId.GHOST_SUBMISSION, ButtonId.FORCE_CLOSE_GAME, ButtonId.ADD_100_NUGGETS };

	public static final Permission[] REQUIRED_PERMISSIONS = new Permission[] { Permission.MESSAGE_MANAGE };

	public static final Currency[] PLAYER_CURRENCIES = new Currency[] { Currency.PUZZLE };

	public static final Currency[] SERVER_CURRENCIES = new Currency[] { Currency.TICKET, Currency.PUZZLE };

	public static final class Resources {
		public static final String HINT_ICON = "https://talanhorne.com/wordpress/wp-content/uploads/2017/12/Animated-Hint-Guy.gif";
		public static final String REWARD_IMAGE = "https://cdn.discordapp.com/attachments/989515060135231510/1007286103046361159/riddlex_puzzle_reward_discord_big.png";
		public static final String FULL_AVATAR_IMAGE = "https://cdn.discordapp.com/attachments/989515060135231510/1007294921440493618/riddlex.png";
		public static final String GIT_REPO = "https://codeberg.org/qbinary/Riddlex";

	}

	public static enum SlashOption {
		submission
	}

	public static enum HexColor {
		ERROR("#BA4F41"), STATUS("#41ACBA"), SUCCESS("#41BA73"), CREATE("#BE1931"), HINT("#E8B19B"), POUCH("#EA596E"),
		QUESTION("#EFBA3E"), LEADEBOARD("#FCD93C"), CLOSED_GAME("#A34027"), REWARD(SUCCESS);

		private Color color;
		private String hex;

		private HexColor(final String hex) {
			this.hex = hex;
			this.color = Color.decode(hex);
		}

		private HexColor(final HexColor color) {
			this.hex = color.hex;
			this.color = color.color;
		}

		public Color getColor() {
			return color;
		}

		@Override
		public String toString() {
			return hex;
		}
	}

	public static final class ErrorText {
		public static final String AI_ERROR_TITLE = "The AI is cringe posting!";
		public static final String AI_ERROR_DESCRIPTION = "The AI responded something invalid. Used currency will be refunded.";
	}

	public static final class UI {
		public static final int MAX_SCOREBOARD_COUNT = 20;
	}

	public static final String LOADING_TEXT = ":gear: Working...";

	public static final int MAX_NAME_LENGTH = 30;

	public static class Game {
		public static final int MIN_SUBMISSIONS = 2;
		public static final long INITIAL_PAYOUT = 15;
		public static final long GAME_TIME = 10000;
	}

	public static final String CONFIG_FILE_PATH = "riddlex.conf";
	public static final long DEV_ID = 327080328239906816L;

	public static final class Embeds {
		public static final String RIDDLE_TITLE = "Riddle :game_die:";

		public static final String SOLUTION_REPLACEMENT_CORRECT = "[HIDDEN]";

		public static final String NO_SUBMISSIONS_TEXT = "no submissions yet";

		public static final String GAME_ALREADY_CLOSED_TITLE = "Game already closed!";
		public static final String GAME_ALREADY_CLOSED_DESCRIPTION = "The game is already closed.";

		public static final String SCOREBOARD_TITLE = "Scoreboard";
		public static final String SCOREBOARD_DESCRIPTION = "";

		public static final String NOT_ENOUGH_RESPONSES_TITLE = ":exclamation: Not enough responses";

		public static final String NOT_ENOUGH_RESPONSES_DESCRIPTION_TEMPLATE = "A minimum of **%d** responses is required currently there are only **%d** responses.";

		public static final String GAME_CLOSED_TITLE = ":white_check_mark: Game Closed!";

		public static final String NOT_ENOUGH_TIME_PASSED_TITLE = ":exclamation: Not enough time passed yet!";
		public static final String NOT_ENOUGH_TIME_PASSED_DESCRIPTION = "";

		public static final String PRICELIST_TITLE = ":euro: Pricelist";

		public static final String RATER_HINT_TITLE = "Rate the Riddle";
		public static final String RATER_HINT_DESCRIPTION = "Rate how solvable the puzzle is and how interesting it is designed. If it is ambiguous and very general, rate it as **Bad**. If the puzzle is very artfully designed and nicely solvable rate it **Excellent**.";

		public static final String LOCAL_LEADERBOARD_TITLE = ":bust_in_silhouette: Local Leaderboard";
		public static final String FEDERATED_LEADERBOARD_TITLE = ":globe_with_meridians: Federated Leaderboard";
		public static final String GLOBAL_LEADERBOARD_TITLE = ":busts_in_silhouette: Global Leaderboard";

		public static final String REWARD_TITLE = "Reward :gift:";
		public static final String REWARD_DESCRIPTION_TEMPLATE = "You got %s!";

		public static final String GAME_RESULT_TITLE = "Game Result :envelope:";

		public static final String INFO_TITLE = "Information about Riddlex :information_source:";

		public static final String ERROR_TITLE = "Error :x:";
	}

	public static final class Text {
		public static final String INITIAL_MESSAGE = "Hi, I see you use me for the first time. Here comes a short introduction:\n"
				+ "\n" + "I create **Riddles** and you can try to solve them!\n"
				+ "If your solution is correct you are rewarded with **puzzles**:jigsaw: and **nuggets**:small_orange_diamond:based on you placement.\n"
				+ "\n"
				+ "Creating a random riddle costs **1 ticket** :tickets:. If you wish to create a riddle which has a custom solution word you need to pay **5 tickets** :tickets: .\n"
				+ "> Create a game with `/play` or `/play [custom word]`\n" + "\n"
				+ "Submitting a solution will cost **1 coin** :coin: .\n" + "> Submit with `/submit [submission]`\n"
				+ "\n"
				+ "Both **coins** :coin: and **tickets** :tickets: can be purchased in the shop with **nuggets** :small_orange_diamond: .\n"
				+ "> Open the shop with `/shop`\n" + "\n"
				+ "There is a global, a local and a federated leaderboard where you can check your and your servers stats.\n"
				+ "> Open leaderboard with `/leaderboard`\n" + "\n" + ":exclamation: **Last but not least**\n"
				+ "> Most of the Actions you take will cost me money because of the GPT-3 API calls in the background. So keep that in mind while using the bot and be gentle to my purse :). \n"
				+ "[Donations will be added in the future.]";

		public static final String HELP_TEXT = ":question: :game_die: **Welcome To Ridley** :game_die: :question: \n"
				+ "__What is it?__\n" + "> Ridley is a riddle game in which every member of a server can compete in. \n"
				+ "\n" + "__How does it work?__\n"
				+ "> I will create a riddle in exchange for a **ticket** :tickets:.  There can only be one riddle at the time.\n"
				+ "> Every member of the Server can submit an answer in exchange for one of their **coins** :coin:.\n"
				+ "> Then I will reward the answers with different amounts of **puzzles** :jigsaw:.\n" + "\n"
				+ "__More about the units__\n"
				+ "> For every **puzzle** :jigsaw: you get, you will also get a **nugget** :small_orange_diamond:. **puzzles** :jigsaw:  are used to indicate the skill of a user while **nuggets** :small_orange_diamond: can be used to buy **coins** :coin: and **tickets** :tickets: in the shop.\n"
				+ "\n" + "> **Tickets** :tickets: are managed per server.\n"
				+ "> **Coins** :coin: and **puzzles** :jigsaw: are managed per user.\n" + "\n" + "__Get started__\n"
				+ "> Just type in **/play** and lets go!";

		public static final String RIDDLE_ALREADY_ACTIVE = "There is already an active riddle!";

		// Buttons
		public static final String SUMMON_CREATE_GAME_BUTTON = "Create game";

		// Submission
		public static final String SUBMISSION_COUNT_PRESET = "**%s** :busts_in_silhouette:";
		public static final String SUBMISSIONS_SUBTITLE = "submissions so far";
		public static final String SUBMIT_TUTORIAL = "Submit an answer with /submit";
		public static final String SUBMISSION_SUCCESSFUL = "For a submission I would ";

		public static final String SUBMISSION_FAILED_TITLE = "Submission failed";
		public static final String SUBMISSION_FAILED_DESCRIPTION = "You probably did not submit anything. Just type in `/submit` and start all over again.";

		public static final String SUBMISSION_SUCCESSFUL_TITLE = "Submission successful!";
		public static final String YOUR_SUBMISSION = "Your submission";

		public static final String APPROVE_SUBMIT_TITLE = "Submit a solution";
		public static final String APPROVE_SUBMIT_DESCRIPTION = "I can submit your solution but I will take **1 coin** :coin: from your pouch.\n\nDo you want me to submit your solution?";

		public static final String ALREADY_SUBMITTED_TITLE = "Only one!";
		public static final String ALREADY_SUBMITTED_DESCRIPTION = "You can only submit one solution per riddle and you already did so. Now all you can do is wait anxiously for the result.";

		// Riddle

		public static final String CREATE_GAME_TITLE = "Create new Game";
		public static final String CREATE_GAME_DESCRIPTION = "There is currently no game!\n"
				+ "I can create one but I will take **1 ticket** :tickets:.\n\n" + "Do you want me to create one?";

		// Pay
		public static final String PAY_AND_CREATE = "Pay and create";
		public static final String PAY_AND_SUBMIT = "Pay and submit";

		public static final String CLOSE_GAME = "Close game";

		public static final String GAME_CLOSED_EXCEPTION = "Game already closed!";

		public static final String RESULTS_TITLE = "Results available!";
		public static final String RESULTS_DESCRIPTION = "The result for the solution you submitted.";

		public static final String NO_GAME_EXCEPTION_TEXT_TEMPLATE = "No game found for guild %d!";

		// No game hint
		public static final String NO_GAME_HINT_TITLE = "There is no active game.";
		public static final String NO_GAME_HINT_DESCRIPTION = "Currently there is no game active. But you can start one!";

		public static final String GAME_DISPLAY_TITLE = "Current Riddle";

		public static final String GUILD_TICKET_COUNT_TEMPLATE = "This server has **%d** :tickets: tickets left!";

		public static final String SERVER_POUCH_TITLE_TEMPLATE = "%s's Pouch :purse:";
		public static final String SERVER_POUCH_DESCRIPTION = "This server has the following resources.";

		public static final String GAME_ALREADY_ACTIVE_TITLE = "Game in progress";
		public static final String GAME_ALREADY_ACTIVE_DESCRIPTION = "There is already a game in progress. Type /play to contribute.";

		public static final String NOT_ENOUGH_TEMPLATE = "Not enough %s!";

		public static final String CLEAR_SUBMISSION_BUTTON = "Delete submission";

		public static final String PREPARED_SUBMISSION_TITLE = "Do you want to submit this?";

		public static final String EMBED_OUTDATED_TITLE = "Embed outdated!";
		public static final String EMBED_OUTDATED_DESCRIPTION = "The menu you reacted to was outdated. You can dismiss it. It does not have any function anymore.";

		public static final String PREPARED_SUBMISSION_CLEARED_TITLE = "Submission cleared!";
		public static final String PREPARED_SUBMISSION_CLEARED_DESCRIPTION = "Your submission was deleted. You can submit a new one with /submit";

		public static final String CORRECT_SOLUTION = ":white_check_mark: Correct!";
		public static final String INCORRECT_SOLUTION = ":x: Incorrect.";

		public static final String PLAYER_POUCH_TITLE_TEMPLATE = "%s's Pouch :purse:";

		public static final String PUZZLES_SUBTITLE = "puzzles";

		public static final String LEADERBOARD_CORRECT_NAME_TEMPLATE = ":small_blue_diamond: %s";
		public static final String LEADERBOARD_INCORRECT_NAME_TEMPLATE = ":small_red_triangle_down: %s";

		public static final String GAME_CREATED_SUCCESSFULLY = ":white_check_mark: Game created successfully!";
	}

	public static final class ID {
		public static final String CREATE_RIDDLE = "create_riddle";
	}

	public static final String RATING_REQUEST = "Rate the response to the riddle on a scale between 0-10. If the riddle was solved correctly it is rated with 10. If the riddle is not solved correctly, the response cannot reach the score 10 but anything from 0-9 depending on the creativity and underlying thought process. While higher creativity and more complex thoughts may result in a better score.\n\nRiddle: %s\n\nSolution: %s\n\nScore: ";

	public static final String RIDDLE_REQUEST = "Create a riddle which solution is the word \"monitor\".\n"
			+ "I am a common household item that is used every day. I come in different shapes and sizes, but I always have a screen. People use me for entertainment and to stay connected. What am I?\n"
			+ "\nCreate a riddle which solution is the word \"%s\".\n";

	public static final long DAILY_DELAY = 8640000;
	public static final long WEEKLY_DELAY = 60480000;
	public static final int RIDDLE_HASH_LENGTH = 40;

	public static final class Command {
		public static final String HELP = "help";
		public static final String PLAY = "play";
		public static final String SUBMIT = "submit";
		public static final String HOME = "home";
		public static final String CLOSE_GAME = "close";
		public static final String SHOP = "shop";
		public static final String LEADERBOARD = "leaderboard";
	}

	public static final class AI {
		public static final String RIDDLE_MODEL = "davinci:ft-personal:ridley-2022-06-09-07-40-42";
		public static final String RATING_MODEL = "text-davinci-002";
	}

	public static final class Submission {
		// PLACEMENT, USERNAME, REWARD
		public static final String SUBMISSION_TO_STRING_TEMPLATE_CORRECT = "**%d. %s** - %d :jigsaw:";

		// USERNAME; WRONG SUBMISSION
		public static final String SUBMISSION_TO_STRING_TEMPLATE_INCORRECT = ":small_red_triangle_down: %s:\"%s\"";
	}

	public static final class Fields {
		public static final String RIDDLE_NAME = "Riddle :game_die:";
		public static final String SOLUTION_NAME = "Solution :ballot_box_with_check:";
		public static final String RESULTS_NAME = "Results :receipt:";
		public static final String SUBMISSION_NAME = "Your Submission :pencil:";
		public static final String REWARD_NAME = "Reward :moneybag:";
		public static final String REWARD_DESCRIPTION = "reward";

		public static final String CORRECT_SUBMISSION_TITLE_TEMPLATE = ":small_blue_diamond: **%d. %s**";
		public static final String CORRECT_SUBMISSION_DESCRIPTION_TEMPLATE = "%d :jigsaw:";

		public static final String INCORRECT_SUBMISSION_TITLE_TEMPLATE = ":small_red_triangle_down: %s";
		public static final String INCORRECT_SUBMISSION_DESCRIPTION_TEMPLATE = "%s";
		public static final String VERSION_NAME_TEMPLATE = "Version %s";
	}

	public static final class SQL {
		//// Players
		// Retrieve
		public static final String GET_PLAYER_BY_ID = "SELECT id, coins, puzzles, nuggets FROM Users WHERE id = ?";
		public static final String GET_PLAYERS = "SELECT id, coins, puzzles, nuggets FROM Users";
		public static final String GET_REGISTERED_PLAYERS = "SELECT Users.id, Users.coins, Users.puzzles, Users.nuggets FROM Users INNER JOIN Members ON Users.id = Members.userId WHERE Members.guildId = ?";

		public static final String GET_PLAYER_COUNT_BY_ID = "SELECT count(*) FROM Users WHERE id = ?";

		// Update
		public static final String CREATE_OR_UPDATE_PLAYER = "INSERT IGNORE INTO Users (id, coins, puzzles, nuggets) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE coins = ?, puzzles = ?, nuggets = ?";
//		public static final String CREATE_PLAYER = "INSERT INTO Users (id, coins, puzzles, nuggets) VALUES (?, ?, ?, ?)";
//		public static final String UPDATE_PLAYER = "UPDATE Users SET coins = ?, puzzles = ?, nuggets = ?";

		public static final String REGISTER_PLAYER_TO_SERVER = "REPLACE INTO Members (userId, guildId) VALUES (?, ?)";

		//// Servers
		// Retrieve
		public static final String GET_SERVER_BY_ID = "SELECT id, tickets, puzzles, riddlexChannel FROM Guilds WHERE id = ?";
		public static final String GET_SERVERS = "SELECT id FROM Guilds";

		// Update
		public static final String CREATE_OR_UPDATE_SERVER = "INSERT IGNORE INTO Guilds (id, tickets, puzzles, riddlexChannel) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE tickets = ?, puzzles = ?, riddlexChannel = ?";

		//// Rating
		public static final String CREATE_OR_UPDATE_RATING = "INSERT INTO Ratings (userId, textHash, rating) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE rating = ?";

		//// Riddle
		public static final String STORE_RIDDLE = "INSERT IGNORE INTO Riddles (text, solution, textHash) VALUES (?, ?, ?)";

	}

	public static final class Discord {
		public static final int MAX_EMBED_FIELD_COUNT = 25;
	}

	public static final class Buttons {
		public static final String BUY_X_TEMPLATE = "Buy %s";
	}
}
