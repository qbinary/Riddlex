package dev.qbin.riddlex.util;

/**
 * Provides an information A by the key B.
 * 
 * @author qbin
 *
 * @param <A> provided type
 * @param <B> key type
 */
public interface LookupProvider<A, B> {
	public A provide(B key);
}
