package dev.qbin.riddlex.util;

public interface Callback<X> {
	public void callback(X event);
}
