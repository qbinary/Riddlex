package dev.qbin.riddlex.util;

public interface Provider<X> {
	public X provide();
}
