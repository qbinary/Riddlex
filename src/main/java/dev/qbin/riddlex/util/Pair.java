package dev.qbin.riddlex.util;

public class Pair<T> {

	public T a;
	public T b;

	public Pair(T a, T b) {
		this.a = a;
		this.b = b;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Pair) {
			Pair<?> pair = (Pair<?>) obj;
			return a.equals(pair.a) && b.equals(pair.b);
		}
		return false;
	}
}
