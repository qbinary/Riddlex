package dev.qbin.riddlex.util.persistence.maria;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.mariadb.jdbc.MariaDbPoolDataSource;

import dev.qbin.riddlex.Constants;
import dev.qbin.riddlex.Constants.SQL;
import dev.qbin.riddlex.bot.tools.GuildNameProvider;
import dev.qbin.riddlex.bot.tools.UsernameProvider;
import dev.qbin.riddlex.model.channel.RiddlexChannel;
import dev.qbin.riddlex.model.channel.RiddlexChannel.RiddlexChannelPermissionException;
import dev.qbin.riddlex.model.currency.Currency;
import dev.qbin.riddlex.model.currency.Wallet;
import dev.qbin.riddlex.model.guild.Guild;
import dev.qbin.riddlex.model.player.Player;
import dev.qbin.riddlex.model.rating.Rating;
import dev.qbin.riddlex.util.UpdateEventEmitter.EventUpdateListener;
import dev.qbin.riddlex.util.persistence.Persistence;
import dev.qbin.riddlex.util.persistence.PersistenceException;
import net.dv8tion.jda.api.JDA;

public class MariaPersistence implements Persistence {

	private MariaDbPoolDataSource pool;

	private ConcurrentHashMap<Long, Player> playerCache = new ConcurrentHashMap<>();
	private ConcurrentHashMap<Long, Guild> serverCache = new ConcurrentHashMap<>();

	private EventUpdateListener<Guild> guildUpdateListener = new EventUpdateListener<Guild>() {
		@Override
		public void onUpdate(Guild updated) {
			try {
				updateServer(updated);
			} catch (PersistenceException e) {
				e.printStackTrace();
			}
		}
	};

//	private EventUpdateListener<Player> playerUpdateListener = new EventUpdateListener<Player>() {
//		@Override
//		public void onUpdate(Player updated) {
//			try {
//				updatePlayer(updated);
//			} catch (PersistenceException e) {
//				e.printStackTrace();
//			}
//		}
//	};

	private final JDA JDA;

	public MariaPersistence(final String spec, final JDA jda) throws SQLException {
		this.JDA = jda;
		pool = new MariaDbPoolDataSource(spec);
	}

	private PreparedStatement exec(final String sql, Object... values) throws SQLException {
		try (Connection db = pool.getConnection()) {
			PreparedStatement statement = db.prepareStatement(sql);
			for (int i = 0; i < values.length; i++) {
				statement.setObject(i + 1, values[i]);
			}
			return statement;
		}
	}

	private Player resultToPlayer(final ResultSet result) throws SQLException {
		Wallet newWallet = new Wallet(Constants.PLAYER_CURRENCIES);
		long userId = result.getLong(1);
		Player resultPlayer = new Player(userId, new UsernameProvider(JDA, userId), newWallet);
		newWallet.registerWalletUpdateListener(new WalletPlayerListenerAdapter(resultPlayer, this));

//		resultPlayer.getWallet().set(Currency.COIN, result.getLong(2));
		resultPlayer.getWallet().set(Currency.PUZZLE, result.getLong(3));
//		resultPlayer.getWallet().set(Currency.NUGGET, result.getLong(4));
		return resultPlayer;
	}

	private Guild resultToServer(final ResultSet result) throws SQLException {
		long guildId = result.getLong(1);

		Wallet newWallet = new Wallet(Constants.SERVER_CURRENCIES);

		RiddlexChannel channel = null;
		long riddlexChannelId = result.getLong(4);
		if (riddlexChannelId > 0)
			// Catch exception for the case that currently there is no access to the
			// RiddlexChannel
			try {
				channel = new RiddlexChannel(JDA, guildId, riddlexChannelId);
			} catch (RiddlexChannelPermissionException e) {
				e.printStackTrace();
			}

		Guild resultServer = new Guild(guildId, newWallet, channel, new GuildNameProvider(JDA, guildId));
		resultServer.addUpdateListener(guildUpdateListener);
		newWallet.registerWalletUpdateListener(new WalletServerListenerAdapter(resultServer, this));
		resultServer.getWallet().set(Currency.TICKET, result.getLong(2));
		resultServer.getWallet().set(Currency.PUZZLE, result.getLong(3));
		return resultServer;
	}

	@Override
	public Player getPlayerById(long userId) throws PersistenceException {
		// Load if not cached
		if (!playerCache.containsKey(userId)) {
			try {
				ResultSet playerQueryResult = exec(SQL.GET_PLAYER_BY_ID, userId).executeQuery();
				Player player = null;
				if (playerQueryResult.next()) {
					player = resultToPlayer(playerQueryResult);
				} else {
					// Create wallet for new player
					Wallet newPlayerWallet = new Wallet(Constants.PLAYER_CURRENCIES);
					player = new Player(userId, new UsernameProvider(JDA, userId), newPlayerWallet);
					newPlayerWallet.registerWalletUpdateListener(new WalletPlayerListenerAdapter(player, this));
					updatePlayer(player);
				}
				playerCache.put(userId, player);
			} catch (SQLException e) {
				throw new PersistenceException(e);
			}
		}

		// Return cache
		return playerCache.get(userId);
	}

	@Override
	public List<Player> getPlayers() throws PersistenceException {
		try {
			// Query players
			ResultSet playersQueryResult = exec(SQL.GET_PLAYERS).executeQuery();

			// Cache all players
			while (playersQueryResult.next()) {
				Player currentPlayer = resultToPlayer(playersQueryResult);
				playerCache.put(currentPlayer.getId(), currentPlayer);
			}
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}

		// Return cache values
		return new LinkedList<>(playerCache.values());
	}

	@Override
	public List<Player> getRegisteredPlayers(long serverId) throws PersistenceException {
		try {
			// Query registered players
			ResultSet registeredPlayersResult = exec(SQL.GET_REGISTERED_PLAYERS, serverId).executeQuery();

			// Add all players to list
			LinkedList<Player> registeredPlayers = new LinkedList<>();
			while (registeredPlayersResult.next()) {
				Player currentPlayer = resultToPlayer(registeredPlayersResult);
				registeredPlayers.add(currentPlayer);
			}

			// Return registered players
			return registeredPlayers;
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
	}

	@Override
	public void updatePlayer(Player player) throws PersistenceException {
		Wallet playerWallet = player.getWallet();
		try {
//			if (isPlayerInDatabase(player.getId())) {
//				// Update, because existent
//				exec(SQL.UPDATE_PLAYER, playerWallet.getCount(Currency.COIN), playerWallet.getCount(Currency.PUZZLE),
//						playerWallet.getCount(Currency.NUGGET)).execute();
//			} else {
//				// Create, because is inexistent
//				exec(SQL.CREATE_PLAYER, player.getId(), playerWallet.getCount(Currency.COIN),
//						playerWallet.getCount(Currency.PUZZLE), playerWallet.getCount(Currency.NUGGET)).execute();
//			}
			// Update cache
			playerCache.put(player.getId(), player);

			// Update db
			long userId = player.getId();
//			long coins = playerWallet.getCount(Currency.COIN);
			long puzzles = playerWallet.getCount(Currency.PUZZLE);
//			long nuggets = playerWallet.getCount(Currency.NUGGET);

			exec(SQL.CREATE_OR_UPDATE_PLAYER, userId, 0, puzzles, 0, 0, puzzles, 0).execute();
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
	}

	@Override
	public void registerPlayerToServer(long userId, long guildId) throws PersistenceException {
		getServerById(guildId);
		try {
			exec(SQL.REGISTER_PLAYER_TO_SERVER, userId, guildId).execute();
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
	}

	@Override
	public boolean isPlayerInDatabase(long userId) throws PersistenceException {
		try {
			ResultSet playerCountResult = exec(SQL.GET_PLAYER_COUNT_BY_ID, userId).executeQuery();
			playerCountResult.next();
			return playerCountResult.getInt(1) == 1;
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
	}

	@Override
	public Guild getServerById(long serverId) throws PersistenceException {
		// Load if not cached
		if (!serverCache.containsKey(serverId)) {
			try {
				ResultSet serverQueryResult = exec(SQL.GET_SERVER_BY_ID, serverId).executeQuery();
				Guild server = null;
				if (serverQueryResult.next()) {
					server = resultToServer(serverQueryResult);
				} else {
					server = new Guild(serverId, new Wallet(Constants.SERVER_CURRENCIES), null, new GuildNameProvider(JDA, serverId));
					server.addUpdateListener(guildUpdateListener);
					updateServer(server);
				}
				serverCache.put(serverId, server);
			} catch (SQLException e) {
				throw new PersistenceException(e);
			}
		}

		// Return cache
		return serverCache.get(serverId);
	}

	@Override
	public List<Guild> getServers() throws PersistenceException {
		try {
			// Query servers
			ResultSet serverQueryResult = exec(SQL.GET_SERVERS).executeQuery();

			// Cache all servers
			while (serverQueryResult.next()) {
				Guild currentServer = getServerById(serverQueryResult.getLong(1));
			}
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}

		// Return cache values
		return new LinkedList<>(serverCache.values());
	}

	@Override
	public void updateServer(Guild server) throws PersistenceException {
		Wallet serverWallet = server.getWallet();
		try {
			// Update cache
			serverCache.put(server.getId(), server);

			// Update db
			long guildId = server.getId();
			long tickets = serverWallet.getCount(Currency.TICKET);
			long puzzles = serverWallet.getCount(Currency.PUZZLE);

			Long channelId = server.getChannel() == null ? null : server.getChannel().getChannelId();

			exec(SQL.CREATE_OR_UPDATE_SERVER, guildId, tickets, puzzles, channelId, tickets, puzzles, channelId)
					.execute();
		} catch (SQLException e) {
			throw new PersistenceException(e);
		}
	}

	@Override
	public void updateRating(Rating rating) throws PersistenceException {
		try {
			short ratingShort = rating.getRating().getRatingValue();

			// TODO workaround to ensure user is in database
			if (!isPlayerInDatabase(rating.getUserId())) {
				getPlayerById(rating.getUserId());
			}

			exec(SQL.CREATE_OR_UPDATE_RATING, rating.getUserId(), rating.getGameHash(), ratingShort, ratingShort)
					.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException(e);
		}
	}

	@Override
	public void storeRiddle(String riddle, String solution, String hash) throws PersistenceException {
		try {
			exec(SQL.STORE_RIDDLE, riddle, solution, hash).execute();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new PersistenceException(e);
		}
	}
}
