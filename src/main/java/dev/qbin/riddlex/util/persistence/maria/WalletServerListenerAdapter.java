package dev.qbin.riddlex.util.persistence.maria;

import dev.qbin.riddlex.model.currency.Wallet.WalletUpdateListener;
import dev.qbin.riddlex.model.guild.Guild;
import dev.qbin.riddlex.util.persistence.Persistence;
import dev.qbin.riddlex.util.persistence.PersistenceException;

public class WalletServerListenerAdapter implements WalletUpdateListener {

	private Guild server;
	private Persistence persistence;

	public WalletServerListenerAdapter(final Guild server, final Persistence persistence) {
		this.server = server;
		this.persistence = persistence;

	}

	@Override
	public void onWalletUpdate() {
		try {
			persistence.updateServer(server);
		} catch (PersistenceException e) {
			e.printStackTrace();
		}
	}

}
