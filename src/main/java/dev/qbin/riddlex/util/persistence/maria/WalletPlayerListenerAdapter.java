package dev.qbin.riddlex.util.persistence.maria;

import dev.qbin.riddlex.model.currency.Wallet.WalletUpdateListener;
import dev.qbin.riddlex.model.player.Player;
import dev.qbin.riddlex.util.persistence.Persistence;
import dev.qbin.riddlex.util.persistence.PersistenceException;

public class WalletPlayerListenerAdapter implements WalletUpdateListener {

	private Player player;
	private Persistence persistence;

	public WalletPlayerListenerAdapter(final Player player, final Persistence persistence) {
		this.player = player;
		this.persistence = persistence;

	}

	@Override
	public void onWalletUpdate() {
		try {
			persistence.updatePlayer(player);
		} catch (PersistenceException e) {
			e.printStackTrace();
			e.getOriginalCause().printStackTrace();
		}
	}

}
