package dev.qbin.riddlex.util.persistence;

import java.util.List;

import dev.qbin.riddlex.model.guild.Guild;
import dev.qbin.riddlex.model.player.Player;
import dev.qbin.riddlex.model.rating.Rating;

// @formatter:off
public interface Persistence {
	//// Player
	// get
	public Player getPlayerById(long userId) throws PersistenceException;
	public List<Player> getPlayers() throws PersistenceException;
	public List<Player> getRegisteredPlayers(long serverId) throws PersistenceException;
	
	// update
	public void updatePlayer(Player player) throws PersistenceException;
	public void registerPlayerToServer(long userId, long guildId) throws PersistenceException;
	
	// meta
	public boolean isPlayerInDatabase(long userId) throws PersistenceException;
	
	//// Server
	// get
	public Guild getServerById(long serverId) throws PersistenceException;
	public List<Guild> getServers()throws PersistenceException;

	// update
	public void updateServer(Guild server) throws PersistenceException;
	
	//// Rating
	public void updateRating(Rating rating) throws PersistenceException;
	
	//// Riddle 
	// store
	public void storeRiddle(String riddle, String solution, String hash) throws PersistenceException;
}
