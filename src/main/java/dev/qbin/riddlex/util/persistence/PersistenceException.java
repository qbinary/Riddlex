package dev.qbin.riddlex.util.persistence;

public class PersistenceException extends RuntimeException {
	private static final long serialVersionUID = 6834506682362564511L;
	private Exception cause;

	public PersistenceException(final Exception cause) {
		super("The persistence service encountered a problem!");
		this.cause = cause;
	}

	public Exception getOriginalCause() {
		return cause;
	}
	
	@Override
	public void printStackTrace() {
		super.printStackTrace();
		cause.printStackTrace();
	}

}
