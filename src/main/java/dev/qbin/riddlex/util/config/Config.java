package dev.qbin.riddlex.util.config;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

public class Config {

	private static enum Key {
		DISCORD_TOKEN, OPENAI_TOKEN, MARIA_SPEC, SCOREBOARD_CACHE_TTL, SCOREBOARD_ENTRIES
	}

	final private Properties properties;

	public Config(final Path configFile) throws IOException {
		properties = new Properties();
		properties.load(Files.newInputStream(configFile));
	}

	private String val(final Key key) {
		String keyString = key.name();
		if (!properties.containsKey(keyString))
			throw new ConfigKeyException(keyString);
		return properties.getProperty(keyString);
	}

	private long valLong(final Key key) {
		try {
			return Long.parseLong(val(key));
		} catch (NumberFormatException e) {
			throw new ConfigKeyException(key.name());
		}
	}

	private int valInt(final Key key) {
		try {
			return Integer.parseInt(val(key));
		} catch (NumberFormatException e) {
			throw new ConfigKeyException(key.name());
		}
	}

	public String getDiscordApiToken() {
		return val(Key.DISCORD_TOKEN);
	}

	public String getOpenAiApiToken() {
		return val(Key.OPENAI_TOKEN);
	}

	public String getMariaSpec() {
		return val(Key.MARIA_SPEC);
	}

	public long getScoreboardTtl() {
		return valLong(Key.SCOREBOARD_CACHE_TTL);
	}

	public int getEntriesPerScoreboard() {
		return valInt(Key.SCOREBOARD_ENTRIES);
	}
}
