package dev.qbin.riddlex.util.config;

public class ConfigKeyException extends RuntimeException {

	private static final long serialVersionUID = 8152427456749332800L;

	public ConfigKeyException(final String key) {
		super(String.format("Configuration key '%s' missing or wrong type!", key));
	}
}
