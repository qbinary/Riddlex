package dev.qbin.riddlex.util.log;

import java.io.PrintStream;
import java.util.Arrays;

public class Log {

	private static boolean verbose = true;

	private Log() {
	}

	public static void setVerbose(boolean verbose) {
		Log.verbose = verbose;
	}

	public static void e(final Object print) {
		err(print, true);
	}

	public static void l(final Object print) {
		out(print, true);
	}

	public static void ef(final String template, final Object... params) {
		err(String.format(template, params), true);
	}

	public static void lf(final String template, final Object... params) {
		out(String.format(template, params), true);
	}

	public static void x(final Exception e) {
		err(String.format("Exception: %s\n%s", e.getClass().getSimpleName(), Arrays.toString(e.getStackTrace())),
				false);
	}

	private static void err(final Object print, boolean ln) {
		print(System.err, print, ln);
	}

	private static void out(final Object print, boolean ln) {
		print(System.out, print, ln);
	}

	private static void print(final PrintStream stream, Object print, boolean ln) {
		if (verbose)
			stream.print(print.toString() + (ln ? "\n" : ""));
	}
}
