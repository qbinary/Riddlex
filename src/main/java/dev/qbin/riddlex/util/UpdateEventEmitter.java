package dev.qbin.riddlex.util;

public interface UpdateEventEmitter<T> {

	public interface EventUpdateListener<T> {
		public void onUpdate(final T updated);
	}

	public void addUpdateListener(final EventUpdateListener<T> listener);
}
