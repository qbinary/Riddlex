package dev.qbin.riddlex.model.scoreboard;

import java.util.LinkedList;
import java.util.List;

public class Scoreboard<T extends Scoreable<T>> {

	protected List<? extends Scoreable<T>> scores;

	public Scoreboard(final List<? extends Scoreable<T>> scores) {
		this.scores = scores;
	}

	public int getPositionOf(final T toDetermine) {
		return getPositionOf(toDetermine.getId());
	}

	public int getPositionOf(final long id) {
		for (final Scoreable<T> i : scores) {
			if (i.getId() == id)
				return scores.indexOf(i);
		}
		return -1;
	}

	public List<? extends Scoreable<T>> getFromStart(int countCap) {
		return copyList(scores, 0, countCap);
	}

	/**
	 * Returns a T {@link List} which has the pivot index in the middle. Result
	 * {@link List} will not exceed the countCap size.
	 */
	public List<T> getArround(int pivot, int countCap) {
		// Ensure the countCap is not exceeded no matter if odd or not
		countCap -= 1;

		// Calculate top and bottom offset
		int delta = Math.floorDiv(countCap, 2);

		// Calculate indices
		int startIndex = pivot - delta;
		int endIndex = pivot + delta + 1;

		// Clamp start index
		if (startIndex < 0)
			startIndex = 0;

		return copyList(scores, startIndex, endIndex);
	}

	/**
	 * @param first inclusive start index
	 * @param end   exclusive end index
	 */
	@SuppressWarnings("unchecked")
	private List<T> copyList(final List<? extends Scoreable<T>> input, int first, int end) {
		List<T> output = new LinkedList<>();
		for (int i = first; i < end && i < scores.size(); i++) {
			output.add((T) input.get(i));
		}
		return output;
	}

}
