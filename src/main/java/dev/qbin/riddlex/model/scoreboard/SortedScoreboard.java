package dev.qbin.riddlex.model.scoreboard;

import java.util.Comparator;
import java.util.List;

public class SortedScoreboard<T extends Scoreable<T>> extends Scoreboard<T> {

	public SortedScoreboard(List<? extends Scoreable<T>> scores) {
		super(scores);
		sort(false);
	}

	public void sort(boolean flip) {
		this.scores.sort(new Comparator<Scoreable<T>>() {
			@Override
			public int compare(Scoreable<T> o1, Scoreable<T> o2) {
				int result = 0;
				if (o1.getScore() < o2.getScore())
					result = 1;
				else
					result = -1;

				if (flip)
					result *= -1;

				return result;
			}
		});
	}

}
