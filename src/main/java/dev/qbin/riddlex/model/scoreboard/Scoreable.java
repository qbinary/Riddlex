package dev.qbin.riddlex.model.scoreboard;

public interface Scoreable<T> {
	public long getScore();
	public long getId();
}
