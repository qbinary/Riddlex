package dev.qbin.riddlex.model.game;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import dev.qbin.riddlex.bot.RiddlexContext;
import dev.qbin.riddlex.model.game.result.DynamicGuildResult;
import dev.qbin.riddlex.model.game.result.GameResult;
import dev.qbin.riddlex.model.game.result.GameResult.UserResult;
import dev.qbin.riddlex.model.game.result.SimpleGameResult;
import dev.qbin.riddlex.model.game.result.SimpleUserResult;
import dev.qbin.riddlex.model.player.User;
import dev.qbin.riddlex.util.Provider;

public class Game {
	private final int REWARD = 1;

	private String id;
	private String riddle;
	private String solution;
	private boolean closed;
	private String hash;
	private String description;

	private HashMap<Long, GameSubmission> submissions = new HashMap<>();
	private LinkedList<GameEventListener> listeners = new LinkedList<>();
	private Provider<String> authorNameProvider;

	private GameResult gameResult = null;

//	private RiddlexContext context;

	public interface GameSubmission {
		public User getAuthor();

		public String getSubmission();

		public long getOriginGuild();
	}

	public Game(final RiddlexContext context, final String riddle, final String solution, final String description,
			final Provider<String> authorNameProvider) {
//		this.context = context;
		this.riddle = riddle;
		this.solution = solution;
		this.description = description;
		this.authorNameProvider = authorNameProvider;
		this.hash = generateHash();

		this.registerGameEventListener(new DmNotificationListener(context.getJda()));
		this.registerGameEventListener(new SaveRewardListener(context));

		context.getPersistence().storeRiddle(riddle, solution, hash);

		try {
			id = generateId(getRiddle().getBytes(), getSolution().getBytes(),
					Long.toString(System.currentTimeMillis()).getBytes());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	public boolean isClosed() {
		return closed;
	}

	public void submit(final GameSubmission submission) {
		submissions.put(submission.getAuthor().getId(), submission);
	}

	public synchronized boolean close() {
		if (!this.closed) {
			this.closed = true;

			LinkedHashMap<Long, DynamicGuildResult> guildResults = new LinkedHashMap<>();
			LinkedList<UserResult> playerResults = new LinkedList<>();

			submissions.keySet().forEach(key -> {
				GameSubmission submission = submissions.get(key);
				boolean correct = isSubmissionCorrect(submission);

				// Add score to guild
				long guildId = submission.getOriginGuild();
				if (!guildResults.containsKey(guildId))
					guildResults.put(guildId, new DynamicGuildResult(guildId, 0));
				guildResults.get(guildId).add(correct ? REWARD : 0);

				// Create user score
				playerResults.add(new SimpleUserResult(submission.getAuthor(), correct ? REWARD : 0,
						submission.getSubmission().toLowerCase().equals(solution.toLowerCase()),
						submission.getSubmission().toLowerCase()));

			});

			this.gameResult = new SimpleGameResult(this, guildResults.values(), playerResults);
			listeners.forEach(l -> l.onClose(gameResult));
			return true;
		}
		return false;
	}

	public String getRiddle() {
		return riddle;
	}

	public String getSolution() {
		return solution;
	}

	public String getDescription() {
		return description;
	}

	public String getId() {
		return id;
	}

	public void registerGameEventListener(final GameEventListener... eventListeners) {
		this.listeners.addAll(Arrays.asList(eventListeners));
	}

	public void unregisterGameEventListener(final GameEventListener... eventListeners) {
		this.listeners.removeAll(Arrays.asList(eventListeners));
	}

	public String getCreator() {
		return authorNameProvider.provide();
	}

	public GameResult getGameResult() {
		return gameResult;
	}

	private String generateId(byte[]... input) throws NoSuchAlgorithmException {
		MessageDigest digest = MessageDigest.getInstance("SHA-1");
		for (final byte[] i : input)
			digest.update(i);
		return Base64.getEncoder().encodeToString(digest.digest());
	}

	public boolean isSubmissionCorrect(final GameSubmission submission) {
		return submission.getSubmission().toLowerCase().equals(solution.toLowerCase());
	}

	public String getHash() {
		return hash;
	}

	private String generateHash() {
		MessageDigest sha;
		try {
			sha = MessageDigest.getInstance("SHA-1");
			byte[] hash = sha.digest((riddle.toLowerCase() + solution.toLowerCase()).getBytes());

			final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
			char[] hexChars = new char[hash.length * 2];
			for (int j = 0; j < hash.length; j++) {
				int v = hash[j] & 0xFF;
				hexChars[j * 2] = HEX_ARRAY[v >>> 4];
				hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
			}
			return new String(hexChars);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

}
