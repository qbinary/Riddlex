package dev.qbin.riddlex.model.game;

import dev.qbin.riddlex.bot.RiddlexContext;
import dev.qbin.riddlex.model.currency.Currency;
import dev.qbin.riddlex.model.game.result.GameResult;
import dev.qbin.riddlex.model.guild.Guild;
import dev.qbin.riddlex.model.player.Player;
import dev.qbin.riddlex.util.persistence.Persistence;

public class SaveRewardListener implements GameEventListener {

	private RiddlexContext context;

	public SaveRewardListener(RiddlexContext context) {
		this.context = context;
	}

	@Override
	public void onClose(GameResult game) {
		Persistence pers = context.getPersistence();
		game.getGuildResults().forEach(result -> {
			Guild guild = pers.getServerById(result.getGuildId());
			guild.getWallet().add(Currency.PUZZLE, result.getReward());
		});

		game.getUserResults().forEach(result -> {
			Player player = pers.getPlayerById(result.getUser().getId());
			if (result.isCorrect())
				player.getWallet().add(Currency.PUZZLE, result.getReward());
		});

	}

}
