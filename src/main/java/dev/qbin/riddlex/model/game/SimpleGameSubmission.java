package dev.qbin.riddlex.model.game;

import dev.qbin.riddlex.model.game.Game.GameSubmission;
import dev.qbin.riddlex.model.player.User;

public class SimpleGameSubmission implements GameSubmission {
	private User user;
	private long guildReferal;
	private String submission;

	public SimpleGameSubmission(final User user, long guildReferal, final String submission) {
		this.user = user;
		this.guildReferal = guildReferal;
		this.submission = submission;
	}

	@Override
	public String getSubmission() {
		return submission;
	}

	@Override
	public User getAuthor() {
		return user;
	}

	@Override
	public long getOriginGuild() {
		return guildReferal;
	}
}
