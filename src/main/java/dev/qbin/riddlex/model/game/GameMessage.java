package dev.qbin.riddlex.model.game;

import dev.qbin.riddlex.bot.ui.Ui;
import dev.qbin.riddlex.bot.ui.factory.UiFactory;
import dev.qbin.riddlex.model.game.result.GameResult;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.ThreadChannel;
import net.dv8tion.jda.api.entities.ThreadChannel.AutoArchiveDuration;
import net.dv8tion.jda.api.managers.channel.concrete.ThreadChannelManager;

public class GameMessage implements GameEventListener {
	private Game game;
	private Message discordMessage;

	public GameMessage(final Message discordMessage, final Game game) {
		this.discordMessage = discordMessage;
		this.game = game;
		game.registerGameEventListener(this);

		Ui ui = UiFactory.newPlainRiddleGameDisplay(game);
		discordMessage.editMessageEmbeds(ui.getEmbeds()).setActionRows(ui.getActionRows()).queue();
	}

	@Override
	public void onClose(GameResult gameResult) {
		// Regenerate ui
		Ui ui = UiFactory.newPlainRiddleGameDisplay(game);
		discordMessage.editMessageEmbeds(ui.getEmbeds()).setActionRows(ui.getActionRows()).queue();

		// Edit thread
		ThreadChannel thread = discordMessage.getChannel().retrieveMessageById(discordMessage.getIdLong()).complete()
				.getStartedThread();
		if (thread != null) {
			ThreadChannelManager manager = thread.getManager();

			manager.setAutoArchiveDuration(AutoArchiveDuration.TIME_1_HOUR).complete();
			manager.setName(String.format("[CLOSED] %s", thread.getName())).complete();
		}
	}

	public Game getGame() {
		return game;
	}

	public Message getDiscordMessage() {
		return discordMessage;
	}
}
