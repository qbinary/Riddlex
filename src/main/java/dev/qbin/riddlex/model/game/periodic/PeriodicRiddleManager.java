package dev.qbin.riddlex.model.game.periodic;

import java.util.LinkedList;

import dev.qbin.riddlex.util.LookupProvider;
import dev.qbin.riddlex.util.Provider;

public class PeriodicRiddleManager extends Thread {

	private LinkedList<PeriodicRiddleUpdateListener> listeners = new LinkedList<>();
	private final long DELAY;

	private String riddle;
	private String solution;

	private Provider<String> wordlist;
	private LookupProvider<String, String> provider;

	public PeriodicRiddleManager(final Provider<String> wordlist, final LookupProvider<String, String> riddleProvider,
			long delay) {
		this.wordlist = wordlist;
		this.provider = riddleProvider;
		this.DELAY = delay;
	}

	private void updateGame() {
		String solution = wordlist.provide();
		this.riddle = provider.provide(solution);
		for (final PeriodicRiddleUpdateListener l : listeners)
			l.onRiddleUpdate(riddle, solution);
	}

	public void registerListener(final PeriodicRiddleUpdateListener listener) {
		listeners.add(listener);
	}

	public void run() {
		while (true) {
			updateGame();
			try {
				Thread.sleep(DELAY);
			} catch (InterruptedException e) {
			}
		}
	}

	public String getCurrentRiddle() {
		return riddle;
	}

	public String getCurrentSolution() {
		return solution;
	}
}
