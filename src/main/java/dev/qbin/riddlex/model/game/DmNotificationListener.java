package dev.qbin.riddlex.model.game;

import dev.qbin.riddlex.bot.ui.Ui;
import dev.qbin.riddlex.bot.ui.factory.UiFactory;
import dev.qbin.riddlex.model.game.result.GameResult;
import net.dv8tion.jda.api.JDA;

class DmNotificationListener implements GameEventListener {
	private JDA jda;

	DmNotificationListener(final JDA jda) {
		this.jda = jda;
	}

	@Override
	public void onClose(GameResult gameresult) {
		gameresult.getUserResults().forEach(userResult -> {
			Ui ui = UiFactory.newGameResult(gameresult.getGame(), userResult);
			jda.retrieveUserById(userResult.getUser().getId()).queue(user -> user.openPrivateChannel().queue(
					channel -> channel.sendMessageEmbeds(ui.getEmbeds()).setActionRows(ui.getActionRows()).queue()));
		});
	}
}
