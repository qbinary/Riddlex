package dev.qbin.riddlex.model.game;

import dev.qbin.riddlex.model.game.result.GameResult;

public interface GameEventListener {
	public void onClose(GameResult game);
}
