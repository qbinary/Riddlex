package dev.qbin.riddlex.model.game.result;

import java.util.Collection;

import dev.qbin.riddlex.model.game.Game;
import dev.qbin.riddlex.model.player.User;

public interface GameResult {
	public interface GuildResult {
		public int getReward();
		public long getGuildId();
	}

	public interface UserResult {
		public User getUser();
		public int getReward();
		public boolean isCorrect();
		public String getSubmission();
	}

	public Game getGame();
	
	public Collection<GuildResult> getGuildResults();
	public Collection<UserResult> getUserResults();
	
	public GuildResult getGuildResult(long guildId);
	public UserResult getUserResult(long userId);
}
