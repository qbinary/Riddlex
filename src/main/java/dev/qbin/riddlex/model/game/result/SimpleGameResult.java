package dev.qbin.riddlex.model.game.result;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import dev.qbin.riddlex.model.game.Game;

@SuppressWarnings("unchecked")
public class SimpleGameResult implements GameResult {

	private Collection<? extends GuildResult> guild;
	private Collection<? extends UserResult> user;

	private Map<Long, GuildResult> guildMap = new HashMap<>();
	private Map<Long, UserResult> userMap = new HashMap<>();
	private final Game GAME;

	public SimpleGameResult(final Game game, Collection<? extends GuildResult> guild,
			Collection<? extends UserResult> user) {
		this.GAME = game;
		this.guild = guild;
		this.user = user;

		guild.forEach(guildResult -> guildMap.put(guildResult.getGuildId(), guildResult));
		user.forEach(userResult -> userMap.put(userResult.getUser().getId(), userResult));
	}

	@Override
	public Collection<GuildResult> getGuildResults() {
		return (Collection<GuildResult>) guild;
	}

	@Override
	public Collection<UserResult> getUserResults() {
		return (Collection<UserResult>) user;
	}

	@Override
	public GuildResult getGuildResult(long guildId) {
		return guildMap.get(guildId);
	}

	@Override
	public UserResult getUserResult(long userId) {
		return userMap.get(userId);
	}

	@Override
	public Game getGame() {
		return GAME;
	}

}
