package dev.qbin.riddlex.model.game.result;

import dev.qbin.riddlex.model.game.result.GameResult.GuildResult;

public class DynamicGuildResult implements GuildResult {

	private long guildId;
	private int result;

	public DynamicGuildResult(long guildId, int initialResult) {
		this.guildId = guildId;
		this.result = initialResult;
	}

	public void add(int count) {
		result += count;
	}

	@Override
	public int getReward() {
		return result;
	}

	@Override
	public long getGuildId() {
		return guildId;
	}

}
