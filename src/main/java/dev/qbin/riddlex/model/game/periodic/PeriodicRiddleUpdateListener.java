package dev.qbin.riddlex.model.game.periodic;

public interface PeriodicRiddleUpdateListener {
	public void onRiddleUpdate(final String riddle, final String solution);
}
