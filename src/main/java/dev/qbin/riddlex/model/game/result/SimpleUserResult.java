package dev.qbin.riddlex.model.game.result;

import dev.qbin.riddlex.model.game.result.GameResult.UserResult;
import dev.qbin.riddlex.model.player.User;

public class SimpleUserResult implements UserResult {

	private User user;
	private int reward;
	private boolean correct;
	private String submission;

	public SimpleUserResult(final User user, int reward, boolean correct, final String submission) {
		this.user = user;
		this.reward = reward;
		this.correct = correct;
		this.submission = submission;
	}

	@Override
	public User getUser() {
		return user;
	}

	@Override
	public int getReward() {
		return reward;
	}

	@Override
	public boolean isCorrect() {
		return correct;
	}

	@Override
	public String getSubmission() {
		return submission;
	}

}
