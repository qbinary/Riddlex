package dev.qbin.riddlex.model.riddle;

public class Riddle {

	private String riddle;
	private String solution;

	public Riddle(final String riddle, final String solution) {
		this.riddle = riddle;
		this.solution = solution;
	}

	public String getRiddle() {
		return riddle;
	}

	public String getSolution() {
		return solution;
	}

	public boolean isCorrect(final String toCheck) {
		return this.solution.toLowerCase().equals(toCheck.toLowerCase());
	}

	@Override
	public String toString() {
		return String.format("%s\n-> %s", riddle, solution);
	}
}
