package dev.qbin.riddlex.model.guild;

import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;

import dev.qbin.riddlex.model.channel.RiddlexChannel;
import dev.qbin.riddlex.model.currency.Currency;
import dev.qbin.riddlex.model.currency.Wallet;
import dev.qbin.riddlex.model.currency.Wallet.WalletUpdateListener;
import dev.qbin.riddlex.model.game.Game;
import dev.qbin.riddlex.model.game.GameMessage;
import dev.qbin.riddlex.model.scoreboard.Scoreable;
import dev.qbin.riddlex.util.Provider;
import dev.qbin.riddlex.util.UpdateEventEmitter;
import net.dv8tion.jda.api.entities.Message;

/**
 * Holds all necessary guild information for Riddlex.
 * 
 * @author qbin
 */
public class Guild implements Scoreable<Guild>, UpdateEventEmitter<Guild>, WalletUpdateListener {

	private long guildId;
	private Wallet wallet;
	private RiddlexChannel channel;

	private LinkedList<EventUpdateListener<Guild>> listeners = new LinkedList<>();

	private ConcurrentHashMap<Long, GameMessage> gameMessages = new ConcurrentHashMap<>();
	private Provider<String> guildNameProvider;

	public Guild(long guildId, final Wallet wallet, final RiddlexChannel channel,
			final Provider<String> guildNameProvider) {
		this.guildId = guildId;
		this.wallet = wallet;
		this.channel = channel;
		this.guildNameProvider = guildNameProvider;
		wallet.registerWalletUpdateListener(this);
	}

	public RiddlexChannel getChannel() {
		return channel;
	}

	public Wallet getWallet() {
		return wallet;
	}

	@Override
	public long getScore() {
		return getWallet().getCount(Currency.PUZZLE);
	}

	@Override
	public long getId() {
		return guildId;
	}

	public void postGame(final Game game) {
		if (channel != null) {
			try {
				Message placeholder = channel.getPlaceholderMessage();
				placeholder.createThreadChannel(game.getDescription()).complete().getManager().setLocked(true)
						.complete();
				GameMessage gmsg = new GameMessage(placeholder, game);
				gameMessages.put(placeholder.getIdLong(), gmsg);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public GameMessage getGamemessageByMessageId(long messageId) {
		return gameMessages.get(messageId);
	}

	private void update() {
		listeners.forEach(listener -> listener.onUpdate(this));
	}

	@Override
	public void onWalletUpdate() {
		update();
	}

	@Override
	public void addUpdateListener(EventUpdateListener<Guild> listener) {
		this.listeners.add(listener);
	}

	public void setChannel(RiddlexChannel riddlexChannel) {
		this.channel = riddlexChannel;
		update();
	}

	@Override
	public String toString() {
		String name = guildNameProvider.provide();
		return String.format("%s - %s", (name == null ? "[inactive]" : name),
				Currency.PUZZLE.printCount(wallet.getCount(Currency.PUZZLE), true));
	}
}
