package dev.qbin.riddlex.model.rating;

public class Rating {
	private final String GAME_HASH;
	private final long USER_ID;
	private final RiddleRating RATING;

	public Rating(final String GAME_HASH, final long USER_ID, final RiddleRating RATING) {
		this.GAME_HASH = GAME_HASH;
		this.USER_ID = USER_ID;
		this.RATING = RATING;
	}

	public String getGameHash() {
		return GAME_HASH;
	}

	public long getUserId() {
		return USER_ID;
	}

	public RiddleRating getRating() {
		return RATING;
	}
}
