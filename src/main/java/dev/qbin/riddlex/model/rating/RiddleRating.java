package dev.qbin.riddlex.model.rating;

public enum RiddleRating {
	PLUS_PLUS((short) 2, "Excellent"), PLUS((short) 1, "Decent"), MINUS((short) -1, "Bad");

	private short ratingValue;
	private String text;

	private RiddleRating(short ratingValue, final String text) {
		this.ratingValue = ratingValue;
		this.text = text;
	}

	public short getRatingValue() {
		return ratingValue;
	}

	@Override
	public String toString() {
		return text;
	}

	public static RiddleRating valueOf(short ratingValue) {
		for (RiddleRating r : RiddleRating.values())
			if (r.getRatingValue() == ratingValue)
				return r;
		return null;
	}
}
