package dev.qbin.riddlex.model.player;

import dev.qbin.riddlex.model.currency.Currency;
import dev.qbin.riddlex.model.currency.Wallet;
import dev.qbin.riddlex.model.scoreboard.Scoreable;
import dev.qbin.riddlex.util.Provider;

/**
 * Holds all necessary player information for Riddlex.
 * 
 * @author qbin
 */
public class Player implements User, Scoreable<Player> {

	public interface PlayerUpdateListener {
		public void onPlayerUpdate(final Player player);
	}

	private Wallet wallet;
	private Provider<String> usernameProvider;
	private long userId;

	public Player(long userId, Provider<String> usernameProvider, final Wallet wallet) {
		this.userId = userId;
		this.usernameProvider = usernameProvider;
		this.wallet = wallet;
	}

	public Wallet getWallet() {
		return wallet;
	}

	@Override
	public String getUsername() {
		return usernameProvider.provide();
	}

	@Override
	public long getScore() {
		return wallet.getCount(Currency.PUZZLE);
	}

	@Override
	public long getId() {
		return userId;
	}

	@Override
	public String toString() {
		return getUsername() + " - " + wallet.getCount(Currency.PUZZLE);
	}

}
