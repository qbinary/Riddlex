package dev.qbin.riddlex.model.player;

public interface User {
	public String getUsername();
	public long getId();
}
