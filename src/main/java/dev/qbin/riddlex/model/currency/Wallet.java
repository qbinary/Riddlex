package dev.qbin.riddlex.model.currency;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import dev.qbin.riddlex.model.guild.Guild;
import dev.qbin.riddlex.model.player.Player;

/**
 * Holds multiple {@link Currency}s for {@link Player} and {@link Guild}
 * 
 * @author qbin
 */
public class Wallet {

	/**
	 * Listener interface which can be registered to a {@link Wallet}
	 * 
	 * @author qbin
	 */
	public interface WalletUpdateListener {
		public void onWalletUpdate();
	}

	private HashMap<Currency, Long> currencies = new HashMap<>();
	private List<WalletUpdateListener> walletUpdateListeners = new LinkedList<>();

	public Wallet() {
	}

	/**
	 * Initializes a wallet with the initial values for the passed {@link Currency}s
	 * 
	 * @param currencies to initialize
	 */
	public Wallet(final Currency... currencies) {
		for (final Currency i : currencies) {
			this.currencies.put(i, i.getInitialCount());
		}
	}

	public void registerWalletUpdateListener(final WalletUpdateListener listener) {
		walletUpdateListeners.add(listener);
	}

	public void unregisterWalletUpdateListener(final WalletUpdateListener listener) {
		walletUpdateListeners.remove(listener);
	}

	public long getCount(final Currency currency) {
		return currencies.get(currency);
	}

	/**
	 * Adds units to a currency
	 * 
	 * @param currency  to add to
	 * @param increment amount to add
	 */
	public void add(final Currency currency, long increment) {
		set(currency, getCount(currency) + increment);
	}

	/**
	 * One can think of this method as a "pay" method.
	 * 
	 * @param currency  to decrement
	 * @param decrement how many units to decrement
	 * @return false if the decrement value would result in the currency count being
	 *         negative / <0
	 */
	public boolean subtract(final Currency currency, long decrement) {
		long currentValue = getCount(currency);
		if (currentValue >= decrement) {
			set(currency, currentValue - decrement);
			return true;
		}
		return false;
	}

	/**
	 * Overrides the value
	 * 
	 * @param currency to set
	 * @param value    to set the count to
	 */
	public void set(final Currency currency, long value) {
		if (getCount(currency) != value) {
			currencies.put(currency, value);
			update();
		}
	}

	private void update() {
		if (walletUpdateListeners.size() > 0)
			walletUpdateListeners.forEach(listener -> listener.onWalletUpdate());
	}
}
