package dev.qbin.riddlex.model.currency;

import dev.qbin.riddlex.bot.tools.Emojo;

/**
 * The currencies which are used by Riddlex
 * 
 * @author qbin
 */
public enum Currency {
	TICKET("Ticket", Emojo.TICKETS, 10), PUZZLE("Puzzle", Emojo.JIGSAW, 0);

	private String name;
	private Emojo emojo;
	private long initialCount;

	private Currency(final String name, final Emojo emojo, long initialCount) {
		this.name = name;
		this.emojo = emojo;
		this.initialCount = initialCount;
	}

	public static String printMultiCurrencyCount(long count, boolean compact, final Currency... currencies) {
		StringBuilder sb = new StringBuilder();
		for (final Currency currency : currencies) {
			sb.append(String.format(" + %s", currency.printCount(count, compact)));
		}
		return sb.toString();
	}

	public String getPlural() {
		return name.toLowerCase() + "s";
	}

	public String printCount(long count, boolean compact) {
		if (compact)
			return String.format("**%d** %s", count, emojo.getEmbedMarkdown());
		else if (count > 1)
			return String.format("**%d** %ss %s", count, name.toLowerCase(), emojo.getEmbedMarkdown());
		else
			return String.format("**%d** %s %s", count, name.toLowerCase(), emojo.getEmbedMarkdown());
	}

	public long getInitialCount() {
		return initialCount;
	}

	public Emojo getEmojo() {
		return emojo;
	}

	@Override
	public String toString() {
		return String.format("%s %s", getPlural(), emojo.getEmbedMarkdown());
	}
}