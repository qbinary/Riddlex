package dev.qbin.riddlex.model.channel;

import dev.qbin.riddlex.bot.ui.factory.EmbedFactory;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;

public class RiddlexChannel {

	public class RiddlexChannelPermissionException extends Exception {
		private static final long serialVersionUID = -3534508351877613931L;
		private Exception e;

		public RiddlexChannelPermissionException(Exception e) {
			super("No access to RiddlexChannel. Kicked from guild or no permission");
			this.e = e;
		}

		@Override
		public void printStackTrace() {
			super.printStackTrace();
			e.printStackTrace();
		}
	}

	private long guildId;
	private long channelId;

	private final JDA JDA;

	public RiddlexChannel(final JDA jda, long guildId, long channelId) throws RiddlexChannelPermissionException {
		this.JDA = jda;
		this.guildId = guildId;
		this.channelId = channelId;
		try {
			applyPermissions();
		} catch (Exception e) {
			throw new RiddlexChannelPermissionException(e);
		}
	}

	public Message getPlaceholderMessage() {
		return JDA.getTextChannelById(channelId).sendMessageEmbeds(EmbedFactory.newLoadingEmbed()).complete();
	}

	/**
	 * Deny: send messages; create private and public threads
	 * 
	 * Allow: send messages in threads, use slash commands
	 */
	public void applyPermissions() {
		Guild guild = JDA.getGuildById(guildId);
		TextChannel riddlexChannel = guild.getChannelById(TextChannel.class, channelId);
		riddlexChannel.upsertPermissionOverride(guild.getPublicRole())
				.deny(Permission.MESSAGE_SEND, Permission.CREATE_PRIVATE_THREADS, Permission.CREATE_PUBLIC_THREADS)
				.grant(Permission.MESSAGE_SEND_IN_THREADS, Permission.USE_APPLICATION_COMMANDS).queue();
		riddlexChannel
				.upsertPermissionOverride(guild.getSelfMember()).grant(Permission.MESSAGE_SEND,
						Permission.CREATE_PUBLIC_THREADS, Permission.CREATE_PRIVATE_THREADS, Permission.MANAGE_THREADS)
				.queue();
	}

	public long getChannelId() {
		return channelId;
	}
}
