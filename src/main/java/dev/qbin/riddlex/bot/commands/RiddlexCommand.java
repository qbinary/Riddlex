package dev.qbin.riddlex.bot.commands;

import net.dv8tion.jda.api.interactions.commands.build.Commands;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;

public enum RiddlexCommand {

	HERE("here", "Configures the current channel as the Riddlex channel.", "here"),
	SUBMIT("submit", "Submit a solution", "submit", RiddlexCommandOption.SUBMISSION),
	INFO("info", "Shows the info page.", "info"), SCOREBOARD("scoreboard", "Shows the scoreboards", "scoreboard"),
	HELP("help", "Opens the help assistant", "help");

	private String name;
	private String description;
	private RiddlexCommandOption[] options;
	private String hook;

	private RiddlexCommand(final String name, final String description, final String hook,
			final RiddlexCommandOption... options) {
		this.name = name;
		this.description = description;
		this.hook = hook;
		this.options = options;
	}

	public SlashCommandData buildCommandData() {
		SlashCommandData data = Commands.slash(name, description);
		for (final RiddlexCommandOption o : options)
			data.addOptions(o.buildOptionData());
		return data;
	}

	public String getPath() {
		return hook;
	}
}
