package dev.qbin.riddlex.bot.commands;

import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;

public enum RiddlexCommandOption {

	SUBMISSION(OptionType.STRING, "submission", "Solution to the riddle.", true);

	private OptionType optionType;
	private String name;
	private String description;
	private boolean required;

	private RiddlexCommandOption(final OptionType optionType, final String name, final String description,
			boolean required) {
		this.optionType = optionType;
		this.name = name;
		this.description = description;
		this.required = required;
	}

	public String getDescription() {
		return description;
	}

	public String getName() {
		return name;
	}

	public OptionType getOptionType() {
		return optionType;
	}

	public OptionData buildOptionData() {
		OptionData data = new OptionData(getOptionType(), getName(), getDescription());
		data.setRequired(required);
		return data;
	}
}
