package dev.qbin.riddlex.bot.feature;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import dev.qbin.riddlex.bot.RiddlexContext;
import dev.qbin.riddlex.util.log.Log;

public class FeatureManager {

	private final RiddlexContext CONTEXT;
	private final List<Feature> FEATURES = new LinkedList<>();

	public FeatureManager(final RiddlexContext context) {
		this.CONTEXT = context;
	}

	public void loadFeatures(Class<?>... activate) throws InstantiationException, IllegalAccessException {
		for (Class<?> i : activate)
			if (!Arrays.asList(i.getInterfaces()).contains(Feature.class)
					&& !Arrays.asList(i.getSuperclass().getInterfaces()).contains(Feature.class)) {
				System.err.println(String.format("%s is not a Feature!", i.getSimpleName()));
			} else {
				Feature currentFeature = (Feature) i.newInstance();
				registerFeature(currentFeature);
			}
	}

	public int unloadFeatures(Class<?>... deactivate) {
		List<Class<?>> deactivateClasses = Arrays.asList(deactivate);

		int unloaded = 0;
		List<Feature> featuresClone = new LinkedList<>(FEATURES);
		for (final Feature feature : featuresClone)
			if (deactivateClasses.contains(feature.getClass())) {
				unregisterFeature(feature);
				unloaded++;
			}

		return unloaded;
	}

	public void enableDebugFeature() {
		registerFeature(new DebugFeature(this));
	}

	public List<Feature> getActiveFeatures() {
		return new LinkedList<>(FEATURES);
	}

	private void unregisterFeature(final Feature feature) {
//		CONTEXT.getJda().removeEventListener(feature);
//		if (Arrays.asList(feature.getClass().getInterfaces()).contains(EventListener.class)
//				|| feature.getClass().getSuperclass().equals(ListenerAdapter.class))
		FEATURES.remove(feature);
		Log.lf("Unloaded feature %s!", feature.getClass().getSimpleName());
	}

	void registerFeature(final Feature feature) {
		feature.load(CONTEXT);
//		if (Arrays.asList(feature.getClass().getInterfaces()).contains(EventListener.class)
//				|| feature.getClass().getSuperclass().equals(ListenerAdapter.class))
//			CONTEXT.getJda().addEventListener(feature);
		FEATURES.add(feature);
		Log.lf("Loaded feature %s!", feature.getClass().getSimpleName());
	}

	public void unloadAllFeatures() {
		FEATURES.forEach(f -> f.unload());
	}
}
