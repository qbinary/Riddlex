package dev.qbin.riddlex.bot.feature.periodic;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import dev.qbin.riddlex.bot.feature.PeriodicRiddleFeature;

public class DailyRiddleFeature extends PeriodicRiddleFeature {
	public DailyRiddleFeature() {
		super(0, Duration.ofDays(1).getSeconds(), TimeUnit.SECONDS, "Daily Riddle");
	}
}
