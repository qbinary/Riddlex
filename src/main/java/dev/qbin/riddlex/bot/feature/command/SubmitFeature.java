package dev.qbin.riddlex.bot.feature.command;

import dev.qbin.riddlex.bot.RiddlexContext;
import dev.qbin.riddlex.bot.commands.RiddlexCommand;
import dev.qbin.riddlex.bot.commands.RiddlexCommandOption;
import dev.qbin.riddlex.bot.feature.slash.SlashCommandFeature;
import dev.qbin.riddlex.bot.feature.slash.SlashCommandFilters;
import dev.qbin.riddlex.bot.ui.TextEmbedContent;
import dev.qbin.riddlex.bot.ui.factory.EmbedFactory;
import dev.qbin.riddlex.model.channel.RiddlexChannel;
import dev.qbin.riddlex.model.game.Game;
import dev.qbin.riddlex.model.game.GameMessage;
import dev.qbin.riddlex.model.game.SimpleGameSubmission;
import dev.qbin.riddlex.model.guild.Guild;
import net.dv8tion.jda.api.entities.Channel;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.ThreadChannel;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.InteractionHook;

public class SubmitFeature extends SlashCommandFeature {

	private RiddlexContext context;

	public SubmitFeature() {
		super(RiddlexCommand.SUBMIT, SlashCommandFilters.getPathFilter(), SlashCommandFilters.getGuildFilter(),
				SlashCommandFilters.getThreadChannelFilter());
	}

	@Override
	public void load(RiddlexContext context) {
		this.context = context;
		context.getJda().addEventListener(this);
	}

	@Override
	public void unload() {
	}

	@Override
	public void onFilteredCommand(SlashCommandInteractionEvent event) {
		long guildId = event.getGuild().getIdLong();

		// Defer reply
		InteractionHook hook = event.deferReply().setEphemeral(true).complete();

		// Check channel
		if (event.getChannelType() == ChannelType.GUILD_PUBLIC_THREAD) {
			ThreadChannel threadChannel = (ThreadChannel) event.getMessageChannel();
			Channel parentChannel = threadChannel.getParentChannel();
			Guild currentGuild = context.getPersistence().getServerById(guildId);
			RiddlexChannel riddlexChannel = currentGuild.getChannel();

			// Check parent channel
			if (riddlexChannel != null && parentChannel.getIdLong() == riddlexChannel.getChannelId()) {
				Guild guild = context.getPersistence().getServerById(guildId);
				GameMessage gmsg = guild
						.getGamemessageByMessageId(threadChannel.retrieveParentMessage().complete().getIdLong());

				// Check if game was found
				if (gmsg != null) {
					Game game = gmsg.getGame();
					if (game != null) {
						// Check if game closed
						if (!game.isClosed()) {
							long userId = event.getUser().getIdLong();
							game.submit(new SimpleGameSubmission(context.getPersistence().getPlayerById(userId),
									guildId, event.getOption(RiddlexCommandOption.SUBMISSION.getName()).getAsString()
											.toLowerCase()));

							hook.editOriginalEmbeds(EmbedFactory.newFromContent(TextEmbedContent.SUBMIT_SUCCESSFUL))
									.queue();
							hook.retrieveOriginal()
									.queue(msg -> msg.getChannel().sendMessage(
											String.format("%s submitted something!", event.getMember().getAsMention()))
											.queue());
						} else {
							hook.editOriginalEmbeds(
									EmbedFactory.newFromContent(TextEmbedContent.SUBMIT_ERROR_GAME_CLOSED)).queue();
						}
					} else {
						hook.editOriginalEmbeds(
								EmbedFactory.newFromContent(TextEmbedContent.SUBMIT_ERROR_NO_GAME_FOUND)).queue();
					}
				} else {
					hook.editOriginalEmbeds(
							EmbedFactory.newFromContent(TextEmbedContent.SUBMIT_ERROR_NO_GAME_FOUND_WEIRD)).queue();
				}
			} else {
				hook.editOriginalEmbeds(EmbedFactory.newFromContent(TextEmbedContent.SUBMIT_ERROR_WRONG_PARENT_CHANNEL))
						.queue();
			}
		} else {
			hook.editOriginalEmbeds(EmbedFactory.newFromContent(TextEmbedContent.SUBMIT_ERROR_WRONG_CHANNEL_TYPE))
					.queue();
		}
	}

}
