package dev.qbin.riddlex.bot.feature;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import dev.qbin.riddlex.bot.RiddlexContext;
import dev.qbin.riddlex.bot.tools.UsernameProvider;
import dev.qbin.riddlex.model.game.Game;
import dev.qbin.riddlex.model.guild.Guild;

public abstract class PeriodicRiddleFeature implements Feature, Runnable {

	private RiddlexContext context;
	private ScheduledExecutorService updateExecutor = Executors.newScheduledThreadPool(1);
	private Game currentGame = null;
	private String description;

	public PeriodicRiddleFeature(long syncDelay, long period, final TimeUnit unit, final String description) {
		this.description = description;
		updateExecutor.scheduleAtFixedRate(this, syncDelay, period, unit);
	}

	@Override
	public void load(RiddlexContext context) {
		this.context = context;
	}

	@Override
	public void unload() {
		closeCurrentGameIfPresent();
	}

	@Override
	public void run() {
		try {
			closeCurrentGameIfPresent();

			// Create new game
			String solution = context.getWordProvider().provide();
			String riddle = context.getRiddleProvider().provide(solution);
			currentGame = new Game(context, riddle, solution,
					String.format("%s %s", description,
							OffsetDateTime.now().format(DateTimeFormatter.ofPattern("DDD·uuuu - HH·mm (O)"))),
					UsernameProvider.createForUser(context.getJda().getSelfUser()));

			// Post new game
			List<Guild> allGuilds;
			allGuilds = context.getPersistence().getServers();
			allGuilds.forEach(guild -> {
				try {
					guild.postGame(currentGame);
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void closeCurrentGameIfPresent() {
		if (currentGame != null)
			currentGame.close();
	}

	public Game getCurrentGame() {
		return currentGame;
	}
}
