package dev.qbin.riddlex.bot.feature.command;

import java.security.InvalidParameterException;

import dev.qbin.riddlex.bot.RiddlexContext;
import dev.qbin.riddlex.bot.commands.RiddlexCommand;
import dev.qbin.riddlex.bot.feature.slash.SlashCommandFeature;
import dev.qbin.riddlex.bot.feature.slash.SlashCommandFilters;
import dev.qbin.riddlex.bot.ui.ButtonId;
import dev.qbin.riddlex.bot.ui.Ui;
import dev.qbin.riddlex.bot.ui.factory.UiFactory;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;

public class HelpFeature extends SlashCommandFeature {

	public HelpFeature() {
		super(RiddlexCommand.HELP, SlashCommandFilters.getPathFilter());
	}

	@Override
	public void load(RiddlexContext context) {
		context.getJda().addEventListener(this);
	}

	@Override
	public void unload() {
	}

	@Override
	public void onFilteredCommand(SlashCommandInteractionEvent event) {
		Ui ui = UiFactory.newHelpUi();
		event.replyEmbeds(ui.getEmbeds()).addActionRows(ui.getActionRows()).setEphemeral(true).queue();
	}

	@Override
	public void onButtonInteraction(ButtonInteractionEvent event) {
		try {
			ButtonId id = ButtonId.valueOf(event.getButton().getId());
			
			switch (id) {
			case HELP_ADMIN:
				event.editMessage("**Hello admin!**\n"
						+ "Thanks for adding me to your server. The setup is pretty simple.\n" + "\n"
						+ "1. Create a new dedicated _Riddlex channel_.\n"
						+ "2. Make sure to give Riddlex following permissions in said _Riddlex channel_:\n"
						+ "  - Read messages\n" + "  - Manage channel\n" + "  - Manage permissions\n"
						+ "  - Manage threads\n"
						+ "3. Use the **`/here`** command to initialize the bot. The bot will configure the channel correctly. \n"
						+ "4. Have fun using the bot.\n" + "(It may take some time for the first riddle to appear)")
						.queue();
				break;

			case HELP_PLAYER:
				event.editMessage("**Hello player!**\n"
						+ "On a server where I am present there has to be a channel dedicated to me. In that channel I post Riddles.\n"
						+ "\n" + "**Play the game :video_game: **\n"
						+ "If you want to participate in one you need to click the **thread** of the riddle you want to solve.\n"
						+ "In that thread you can then **submit** a solution with the command\n"
						+ "**`/submit [submission]`**\n" + "\n"
						+ "You can only have **one** submission per riddle. But you are able to **replace your submission** at anytime while the game is still on, just by using the submit command again!\n"
						+ "\n" + "**Scoreboard**\n"
						+ "To open the scoreboard just type in any **channel** or **direct message** \n"
						+ "**`/scoreboard`**\n" + "\n" + "**Information**\n"
						+ "You can also see some information about the bot by using this command in a **channel** or **direct messsage**\n"
						+ "**`/info`**\n" + "\n" + "**:exclamation:DISCLAIMER:exclamation:**\n"
						+ "The bot is still in development! So there can be bugs. If you want to help in the development go to \n"
						+ "https://codeberg.org/qbinary/Riddlex\n" + "\n"
						+ "We also have a **Discord** there where you can test latest developer builds and participate in decision processes for new features and improvements. Feel free to join. (The invite is on the Codeberg page)")
						.queue();
				break;

			default:
				break;
			}
		} catch (InvalidParameterException e) {
			e.printStackTrace();
		}
	}
}
