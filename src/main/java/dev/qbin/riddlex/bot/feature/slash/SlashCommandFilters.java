package dev.qbin.riddlex.bot.feature.slash;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import dev.qbin.riddlex.bot.commands.RiddlexCommand;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class SlashCommandFilters {

	public interface SlashCommandFilter {
		public boolean filter(SlashCommandInteractionEvent event, RiddlexCommand command);

		public String getFailDescription();
	}

	private static SlashCommandFilter pathFilter = new SlashCommandFilter() {
		@Override
		public boolean filter(SlashCommandInteractionEvent event, RiddlexCommand command) {
			return event.getCommandPath().equals(command.getPath());
		}

		@Override
		public String getFailDescription() {
			return null;
		}
	};

	private static SlashCommandFilter guildFilter = new SlashCommandFilter() {
		@Override
		public boolean filter(SlashCommandInteractionEvent event, RiddlexCommand command) {
			return event.isFromGuild();
		}

		@Override
		public String getFailDescription() {
			return "Command must be executed on a server!";
		}
	};
	
	private static SlashCommandFilter threadChannelFilter = new SlashCommandFilter() {
		@Override
		public boolean filter(SlashCommandInteractionEvent event, RiddlexCommand command) {
			return event.getChannelType() == ChannelType.GUILD_PUBLIC_THREAD;
		}

		@Override
		public String getFailDescription() {
			return "Command must be executed in a thread!";
		}
	};

	public class UserWhitelistFilter implements SlashCommandFilter {
		public List<Long> whitelist;

		private UserWhitelistFilter(Long... whitelist) {
			this.whitelist = new LinkedList<Long>(Arrays.asList(whitelist));
		}

		@Override
		public boolean filter(SlashCommandInteractionEvent event, RiddlexCommand command) {
			return whitelist.contains(event.getUser().getIdLong());
		}

		@Override
		public String getFailDescription() {
			return "You are not allowed to execute this command!";
		}

	}

	public static SlashCommandFilter getPathFilter() {
		return pathFilter;
	}

	public static SlashCommandFilter getGuildFilter() {
		return guildFilter;
	}

	public static SlashCommandFilter getThreadChannelFilter() {
		return threadChannelFilter;
	}
}
