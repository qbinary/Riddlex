package dev.qbin.riddlex.bot.feature.misc;

import java.util.List;

import dev.qbin.riddlex.Constants;
import dev.qbin.riddlex.bot.RiddlexContext;
import dev.qbin.riddlex.bot.feature.Feature;
import dev.qbin.riddlex.bot.tools.DiscordTextTool;
import dev.qbin.riddlex.bot.ui.ButtonId;
import dev.qbin.riddlex.bot.ui.TextEmbedContent;
import dev.qbin.riddlex.bot.ui.factory.EmbedFactory;
import dev.qbin.riddlex.model.rating.Rating;
import dev.qbin.riddlex.model.rating.RiddleRating;
import dev.qbin.riddlex.util.persistence.PersistenceException;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.component.ButtonInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.InteractionHook;

public class RatingFeature extends ListenerAdapter implements Feature {

	private RiddlexContext context;

	@Override
	public void load(RiddlexContext context) {
		this.context = context;
		context.getJda().addEventListener(this);
	}

	@Override
	public void unload() {
	}

	@Override
	public void onButtonInteraction(ButtonInteractionEvent event) {
		ButtonId buttonId = null;
		try {
			buttonId = ButtonId.valueOf(event.getButton().getId());
		} catch (Exception e) {
		}

		if (buttonId != null
				&& (buttonId == ButtonId.MINUS || buttonId == ButtonId.PLUS || buttonId == ButtonId.PLUS_PLUS)) {
			Message msg = event.getMessage();
			InteractionHook hook = event.deferReply().complete();
			if (msg.getAuthor().getIdLong() == event.getJDA().getSelfUser().getIdLong()) {
				String riddleHash = null;
				List<MessageEmbed> embeds = msg.getEmbeds();
				for (final MessageEmbed EMBED : embeds) {
					String footer = EMBED.getFooter() != null ? EMBED.getFooter().getText() : null;
					if (footer != null && footer.length() == Constants.RIDDLE_HASH_LENGTH) {
						riddleHash = footer;
					}
				}

				if (riddleHash != null) {
					try {
						context.getPersistence().updateRating(new Rating(riddleHash, event.getUser().getIdLong(),
								RiddleRating.valueOf(event.getButton().getId())));
						hook.editOriginalEmbeds(EmbedFactory.newFromContent(TextEmbedContent.RATE_SUCCESS)).queue();
					} catch (PersistenceException e) {
						hook.editOriginal(DiscordTextTool.formatException(e, true)).queue();
					}
				} else {
					hook.editOriginalEmbeds(EmbedFactory.newFromContent(TextEmbedContent.RATE_ERROR_INVALID_MESSAGE))
							.queue();
				}
			}
		}
	}

}
