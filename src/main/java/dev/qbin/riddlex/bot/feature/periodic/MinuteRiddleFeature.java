package dev.qbin.riddlex.bot.feature.periodic;

import java.util.concurrent.TimeUnit;

import dev.qbin.riddlex.bot.feature.PeriodicRiddleFeature;

public class MinuteRiddleFeature extends PeriodicRiddleFeature {
	public MinuteRiddleFeature() {
		super(0, 20, TimeUnit.SECONDS, "SPECIAL EVENT RIDDLE");
	}
}