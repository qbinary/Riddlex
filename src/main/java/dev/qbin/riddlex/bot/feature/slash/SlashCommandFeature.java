package dev.qbin.riddlex.bot.feature.slash;

import java.util.Arrays;
import java.util.LinkedList;

import dev.qbin.riddlex.bot.commands.RiddlexCommand;
import dev.qbin.riddlex.bot.feature.Feature;
import dev.qbin.riddlex.bot.feature.slash.SlashCommandFilters.SlashCommandFilter;
import dev.qbin.riddlex.bot.ui.factory.EmbedFactory;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public abstract class SlashCommandFeature extends ListenerAdapter implements Feature {

	private final RiddlexCommand COMMAND;
	private final LinkedList<SlashCommandFilter> filters = new LinkedList<>();

	public SlashCommandFeature(final RiddlexCommand command, SlashCommandFilter... filters) {
		this.COMMAND = command;
		this.filters.addAll(Arrays.asList(filters));
	}

	@Override
	public void onSlashCommandInteraction(SlashCommandInteractionEvent event) {
		SlashCommandFilter failedFilter = null;
		for (final SlashCommandFilter filter : filters) {
			if (!filter.filter(event, COMMAND)) {
				failedFilter = filter;
				break;
			}
		}

		if (failedFilter == null) {
			onFilteredCommand(event);
		} else {
			String failMessage = failedFilter != null ? failedFilter.getFailDescription() : null;
			if (failMessage != null)
				event.replyEmbeds(EmbedFactory.newError(failMessage)).queue();
		}
	}

	public abstract void onFilteredCommand(SlashCommandInteractionEvent event);
}
