package dev.qbin.riddlex.bot.feature;

import dev.qbin.riddlex.bot.RiddlexContext;

public interface Feature {
	void load(RiddlexContext context);
	void unload();
}
