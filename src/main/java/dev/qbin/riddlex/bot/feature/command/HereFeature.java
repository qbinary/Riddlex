package dev.qbin.riddlex.bot.feature.command;

import dev.qbin.riddlex.Constants;
import dev.qbin.riddlex.bot.RiddlexContext;
import dev.qbin.riddlex.bot.commands.RiddlexCommand;
import dev.qbin.riddlex.bot.feature.slash.SlashCommandFeature;
import dev.qbin.riddlex.bot.feature.slash.SlashCommandFilters;
import dev.qbin.riddlex.bot.ui.TextEmbedContent;
import dev.qbin.riddlex.bot.ui.factory.EmbedFactory;
import dev.qbin.riddlex.model.channel.RiddlexChannel;
import dev.qbin.riddlex.model.channel.RiddlexChannel.RiddlexChannelPermissionException;
import dev.qbin.riddlex.model.guild.Guild;
import dev.qbin.riddlex.util.persistence.PersistenceException;
import net.dv8tion.jda.api.entities.Channel;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.InteractionHook;

public class HereFeature extends SlashCommandFeature {

	private RiddlexContext context;

	public HereFeature() {
		super(RiddlexCommand.HERE, SlashCommandFilters.getPathFilter());
	}

	@Override
	public void load(RiddlexContext context) {
		this.context = context;
		context.getJda().addEventListener(this);
	}

	@Override
	public void unload() {
	}

	@Override
	public void onFilteredCommand(SlashCommandInteractionEvent event) {
		try {
			InteractionHook hook = event.deferReply().complete();

			long guildId = event.getGuild().getIdLong();

			// Ensure correct channel type => TEXT (fuer Max)
			Channel channel = event.getChannel();
			if (channel.getType() == ChannelType.TEXT) {
				// Retrieve guild
				Guild guild = context.getPersistence().getServerById(event.getGuild().getIdLong());

				RiddlexChannel riddlexChannel;
				try {
					riddlexChannel = new RiddlexChannel(context.getJda(), guildId, channel.getIdLong());
					guild.setChannel(riddlexChannel);

					hook.editOriginalEmbeds(EmbedFactory.newFromContent(TextEmbedContent.HERE_SUCCESS)).queue();
				} catch (RiddlexChannelPermissionException e) {
					if (Constants.DEBUG_BUILD)
						e.printStackTrace();
					hook.editOriginalEmbeds(EmbedFactory.newFromContent(TextEmbedContent.HERE_ERROR_NO_CHANNEL_ACCESS))
							.queue();
				}
			} else {
				hook.editOriginalEmbeds(EmbedFactory.newFromContent(TextEmbedContent.HERE_ERROR_WRONG_CHANNEL_TYPE))
						.queue();
			}
		} catch (PersistenceException e) {
			e.printStackTrace();
		}
	}

}
