package dev.qbin.riddlex.bot.feature.command;

import dev.qbin.riddlex.bot.RiddlexContext;
import dev.qbin.riddlex.bot.commands.RiddlexCommand;
import dev.qbin.riddlex.bot.feature.slash.SlashCommandFeature;
import dev.qbin.riddlex.bot.feature.slash.SlashCommandFilters;
import dev.qbin.riddlex.bot.ui.Ui;
import dev.qbin.riddlex.bot.ui.factory.UiFactory;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class InformationFeature extends SlashCommandFeature {

	public InformationFeature() {
		super(RiddlexCommand.INFO, SlashCommandFilters.getPathFilter());
	}

	@Override
	public void load(RiddlexContext context) {
		context.getJda().addEventListener(this);
	}

	@Override
	public void unload() {
	}

	@Override
	public void onFilteredCommand(SlashCommandInteractionEvent event) {
		Ui ui = UiFactory.newInfoUi();
		event.replyEmbeds(ui.getEmbeds()).addActionRows(ui.getActionRows()).setEphemeral(true).queue();
	}
}
