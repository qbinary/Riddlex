package dev.qbin.riddlex.bot.feature;

import java.util.Arrays;

import dev.qbin.riddlex.Constants;
import dev.qbin.riddlex.bot.RiddlexContext;
import dev.qbin.riddlex.bot.tools.DiscordTextTool;
import dev.qbin.riddlex.bot.tools.UsernameProvider;
import dev.qbin.riddlex.model.currency.Currency;
import dev.qbin.riddlex.model.game.Game;
import dev.qbin.riddlex.model.game.GameMessage;
import dev.qbin.riddlex.model.guild.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class DebugFeature extends ListenerAdapter implements Feature {

	private enum DebugCommand {
		FTLD("Sideloads a feature at runtime."), FTUNLD("Unloads a feature at runtime."),
		FTLS("Lists all active features"), VER("Shows the current bot version."), HELP("Shows this help screen."),
		STOP("Stops the bot."), CLRTHREADS("Clears all threads from the channel."), GMCLOSE("Closes the current game."),
		GMSOLUTION("Shows the game solution."), GMNEW("Create new game."), PLPUZZ("Lists a players puzzles.");

		private String helpText;

		private DebugCommand(final String help) {
			helpText = help;
		}

		public String getHelpText() {
			return helpText;
		}

		public static String buildHelpText() {
			StringBuilder helpTextBuilder = new StringBuilder();
			for (final DebugCommand command : values()) {
				helpTextBuilder.append(String.format(":small_blue_diamond: **`%s`**- %s\n",
						command.name().toLowerCase(), command.getHelpText()));
			}
			return helpTextBuilder.toString();
		}
	}

	private RiddlexContext context;
	private FeatureManager featureManager;

	DebugFeature(FeatureManager featureManager) {
		this.featureManager = featureManager;
	}

	@Override
	public void load(RiddlexContext context) {
		this.context = context;
		context.getJda().addEventListener(this);
	}

	@Override
	public void onMessageReceived(MessageReceivedEvent event) {
		Message msg = event.getMessage();
		String content = msg.getContentStripped();
		if (msg.getAuthor().getIdLong() == Constants.DEV_ID) {
			// prefix
			if (content.startsWith("rdx ")) {
				String[] inputsplit = content.substring(4).split(" ");
				String command = inputsplit[0];
				String[] params = Arrays.copyOfRange(inputsplit, 1, inputsplit.length);

				DebugCommand wrappedCommand = DebugCommand.HELP;
				try {
					wrappedCommand = DebugCommand.valueOf(command.toUpperCase());
				} catch (Exception e) {
				}

				try {
					switch (wrappedCommand) {
					case FTLD:
						Feature tempFeature = (Feature) Class.forName(params[0]).newInstance();
						featureManager.registerFeature(tempFeature);
						msg.reply("Feature sideload successful!").queue();
						break;

					case FTUNLD:
						int unloaded = featureManager
								.unloadFeatures(ClassLoader.getSystemClassLoader().loadClass(params[0]));
						msg.reply(String.format("Unloaded %d features!", unloaded)).queue();
						break;

					case FTLS:
						StringBuilder sb = new StringBuilder("**Enabled Features** :gear:\n");
						featureManager.getActiveFeatures().forEach(
								f -> sb.append(String.format(":small_blue_diamond: `%s`\n", f.getClass().getName())));
						event.getMessage().reply(sb.toString()).queue();
						break;
					case STOP:
						context.stop();
						break;

					case VER:
						event.getMessage().reply(DiscordTextTool.codeInline(Constants.FOOTPRINT)).queue();
						break;

					case HELP:
						event.getMessage().reply(DebugCommand.buildHelpText()).queue();
						break;

					case CLRTHREADS:
						event.getChannel().asTextChannel().getThreadChannels()
								.forEach(thread -> thread.delete().queue());
						break;

					case GMCLOSE: {
						long targetMessage = event.getGuildChannel().asThreadChannel().retrieveParentMessage()
								.complete().getIdLong();
						Guild guild = context.getPersistence().getServerById(event.getGuild().getIdLong());
						GameMessage gmsg = guild.getGamemessageByMessageId(targetMessage);
						gmsg.getGame().close();
					}
						break;

					case GMSOLUTION: {
						long targetMessage = event.getGuildChannel().asThreadChannel().retrieveParentMessage()
								.complete().getIdLong();
						Guild guild = context.getPersistence().getServerById(event.getGuild().getIdLong());
						GameMessage gmsg = guild.getGamemessageByMessageId(targetMessage);
						event.getMessage().reply(gmsg.getGame().getSolution()).queue();
					}
						break;

					case GMNEW:
						Guild guild = context.getPersistence().getServerById(event.getGuild().getIdLong());
						guild.postGame(new Game(context, params[0], params[1], params[2],
								UsernameProvider.createForUser(context.getJda().getSelfUser())));
						break;

					case PLPUZZ:
						event.getMessage().reply(Long.toString(context.getPersistence()
								.getPlayerById(Long.parseLong(params[0])).getWallet().getCount(Currency.PUZZLE)))
								.queue();
						break;
					}

				} catch (Exception e) {
					e.printStackTrace();
					event.getMessage().reply(DiscordTextTool.formatException(e, true)).queue();
				}
			}
		}
	}

	@Override
	public void unload() {
	}
}
