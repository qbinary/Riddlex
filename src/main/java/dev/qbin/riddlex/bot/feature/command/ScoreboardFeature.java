package dev.qbin.riddlex.bot.feature.command;

import java.util.List;

import dev.qbin.riddlex.bot.RiddlexContext;
import dev.qbin.riddlex.bot.commands.RiddlexCommand;
import dev.qbin.riddlex.bot.feature.slash.SlashCommandFeature;
import dev.qbin.riddlex.bot.feature.slash.SlashCommandFilters;
import dev.qbin.riddlex.bot.ui.Ui;
import dev.qbin.riddlex.bot.ui.factory.UiFactory;
import dev.qbin.riddlex.model.guild.Guild;
import dev.qbin.riddlex.model.player.Player;
import dev.qbin.riddlex.model.scoreboard.Scoreboard;
import dev.qbin.riddlex.model.scoreboard.SortedScoreboard;
import dev.qbin.riddlex.util.persistence.Persistence;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;

public class ScoreboardFeature extends SlashCommandFeature {
	private long cacheTtl;

	private RiddlexContext context;
	private long validUntil = -1;

	private Scoreboard<Guild> federatedGuild;
	private Scoreboard<Player> globalPlayer;

	public ScoreboardFeature() {
		super(RiddlexCommand.SCOREBOARD, SlashCommandFilters.getPathFilter());
	}

	@Override
	public void load(final RiddlexContext context) {
		this.context = context;
		context.getJda().addEventListener(this);
		cacheTtl = context.getConfig().getScoreboardTtl();
	}

	@Override
	public void unload() {
	}

	private void updateScoreboards() {
		if (System.currentTimeMillis() > validUntil) {
			Persistence pers = context.getPersistence();

			List<Guild> allGuilds = pers.getServers();
			this.federatedGuild = new SortedScoreboard<>(allGuilds);

			List<Player> allPlayers = pers.getPlayers();
			this.globalPlayer = new SortedScoreboard<>(allPlayers);

			validUntil = System.currentTimeMillis() + cacheTtl;
		}
	}

	@Override
	public void onFilteredCommand(SlashCommandInteractionEvent event) {
		updateScoreboards();

		List<Player> localPlayers = context.getPersistence().getRegisteredPlayers(event.getGuild().getIdLong());
		SortedScoreboard<Player> localPlayerBoard = new SortedScoreboard<>(localPlayers);

		Ui scoreboard = UiFactory.newLeaderboardsView(localPlayerBoard, federatedGuild, globalPlayer,
				event.getUser().getIdLong(), event.getGuild().getIdLong(),
				context.getConfig().getEntriesPerScoreboard());

		event.replyEmbeds(scoreboard.getEmbeds()).addActionRows(scoreboard.getActionRows()).setEphemeral(true).queue();
	}
}
