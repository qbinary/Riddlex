package dev.qbin.riddlex.bot.feature.periodic;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import dev.qbin.riddlex.bot.feature.PeriodicRiddleFeature;

public class WeeklyRiddleFeature extends PeriodicRiddleFeature {
	public WeeklyRiddleFeature() {
		super(0, Duration.ofDays(7).getSeconds(), TimeUnit.SECONDS, "Weekly Riddle");
	}
}