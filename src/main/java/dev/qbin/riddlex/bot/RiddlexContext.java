package dev.qbin.riddlex.bot;

import dev.qbin.riddlex.bot.feature.FeatureManager;
import dev.qbin.riddlex.util.LookupProvider;
import dev.qbin.riddlex.util.Provider;
import dev.qbin.riddlex.util.config.Config;
import dev.qbin.riddlex.util.persistence.Persistence;
import dev.qbin.riddlex.util.persistence.PersistenceException;
import net.dv8tion.jda.api.JDA;

public class RiddlexContext {
	private JDA jda;

	private Persistence persistence;
	private Provider<String> wordProvider;
	private LookupProvider<String, String> riddleProvider;

	private FeatureManager featureManager;

	private Config config;

	public RiddlexContext(final JDA jda, final Persistence persistence, final Provider<String> wordProvider,
			final LookupProvider<String, String> riddleProvider, final Config config) throws PersistenceException {
		this.jda = jda;
		this.persistence = persistence;
		this.wordProvider = wordProvider;
		this.riddleProvider = riddleProvider;
		this.config = config;
		this.featureManager = new FeatureManager(this);
	}

	public JDA getJda() {
		return jda;
	}

	public Persistence getPersistence() {
		return persistence;
	}

	public LookupProvider<String, String> getRiddleProvider() {
		return riddleProvider;
	}

	public Provider<String> getWordProvider() {
		return wordProvider;
	}

	public FeatureManager getFeatureManager() {
		return featureManager;
	}

	public Config getConfig() {
		return config;
	}

	public void stop() {
		featureManager.unloadAllFeatures();
		jda.shutdown();
		System.exit(0);
	}
}
