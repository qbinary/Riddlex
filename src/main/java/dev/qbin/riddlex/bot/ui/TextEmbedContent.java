package dev.qbin.riddlex.bot.ui;

import dev.qbin.riddlex.Constants.HexColor;

public enum TextEmbedContent {
	ERROR("Error :x:", null, HexColor.ERROR), HINT("Hint :bulb:", null, HexColor.HINT),
	SUCCESS("Success :white_check_mark:", null, HexColor.SUCCESS),
	HERE_ERROR_WRONG_CHANNEL_TYPE(ERROR.getTitle(), "This is not a genuine text channel!", ERROR.getColor()),
	HERE_SUCCESS(SUCCESS.getTitle(), "The channel is now registered! Permission overrides applied.",
			SUCCESS.getColor()),
	SUBMIT_ERROR_WRONG_CHANNEL_TYPE(ERROR.getTitle(),
			"You cannot submit here! You need to go into to the thread of the riddle you want to solve.",
			ERROR.getColor()),
	SUBMIT_ERROR_WRONG_PARENT_CHANNEL(ERROR.getTitle(),
			"You cannot submit here! The parent channel is not the Riddlex channel.", ERROR.getColor()),
	SUBMIT_ERROR_NO_GAME_FOUND(ERROR.getTitle(), "This channel is not part of any active game.", ERROR.getColor()),
	SUBMIT_ERROR_GAME_CLOSED(ERROR.getTitle(), "You cannot submit to this closed game.", ERROR.getColor()),
	SUBMIT_SUCCESSFUL(SUCCESS.getTitle(), "Submitted successfully!", SUCCESS.getColor()),
	SUBMIT_ERROR_DUPLICATE_SUBMISSION(ERROR.getTitle(), "You already submitted to this game.", ERROR.getColor()),
	THREAD_SUBMISSION_HINT(HINT.getTitle(), "You can submit here with \n`/submit [submission]`.", HINT.getColor()),
	HERE_ERROR_NO_CHANNEL_ACCESS(ERROR.getTitle(), "No access to the RiddlexChannel! Make sure MANAGE_CHANNEL and MANAGE_PERMISSIONS is granted for me.", ERROR.getColor()),
	GAME_RESULTS_READY("New game results!", "The results of a riddle you participated in are available!",
			SUCCESS.getColor()),
	RATE_ERROR_INVALID_MESSAGE(ERROR.getTitle(), "The system cannot find this message!", ERROR.getColor()),
	RATE_SUCCESS(SUCCESS.getTitle(), "You rating was saved. If you rated already the old rating is overridden.",
			SUCCESS.getColor()), SUBMIT_ERROR_NO_GAME_FOUND_WEIRD(ERROR.getTitle(), "Cannot find the game registered to this channel!", ERROR.getColor()), HELP_UI_START("Help Pages", "Select a page below", SUCCESS.getColor());

	private String title;
	private String description;
	private HexColor color;

	private TextEmbedContent(final String title, final String description, final HexColor color) {
		this.title = title;
		this.description = description;
		this.color = color;
	}

	public HexColor getColor() {
		return color;
	}

	public String getDescription() {
		return description;
	}

	public String getTitle() {
		return title;
	}
}
