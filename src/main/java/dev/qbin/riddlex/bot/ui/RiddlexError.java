package dev.qbin.riddlex.bot.ui;

import dev.qbin.riddlex.Constants.ErrorText;

public enum RiddlexError {
	AI_ERROR(ErrorText.AI_ERROR_TITLE, ErrorText.AI_ERROR_TITLE);

	private String errorTitle;
	private String errorDescription;

	private RiddlexError(final String errorTitle, final String errorDescription) {
		this.errorTitle = errorTitle;
		this.errorDescription = errorDescription;
	}

	public String getErrorDescription() {
		return errorDescription;
	}

	public String getErrorTitle() {
		return errorTitle;
	}

	@Override
	public String toString() {
		return String.format("%s: %s", getErrorTitle(), getErrorDescription());
	}
}
