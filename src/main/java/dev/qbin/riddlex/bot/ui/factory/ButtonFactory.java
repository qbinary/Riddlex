package dev.qbin.riddlex.bot.ui.factory;

import dev.qbin.riddlex.Constants.Text;
import dev.qbin.riddlex.bot.tools.Emojo;
import dev.qbin.riddlex.bot.ui.ButtonId;
import dev.qbin.riddlex.model.currency.Currency;
import dev.qbin.riddlex.model.rating.RiddleRating;
import net.dv8tion.jda.api.interactions.components.buttons.Button;

public class ButtonFactory {
	private ButtonFactory() {
	}

	private static Button secondaryButton(final ButtonId id, final String text, final Emojo emojo) {
		Button button = Button.secondary(id.name(), text);
		if (emojo != null)
			button = button.withEmoji(emojo.getEmoji());
		return button;
	}

	private static Button dangerButton(final ButtonId id, final String text, final Emojo emojo) {
		Button button = Button.danger(id.name(), text);
		if (emojo != null)
			button = button.withEmoji(emojo.getEmoji());
		return button;
	}

	private static Button successButton(final ButtonId id, final String text, final Emojo emojo) {
		Button button = Button.success(id.name(), text);
		if (emojo != null)
			button = button.withEmoji(emojo.getEmoji());
		return button;
	}

	public static Button newPayAndCreateButton(final ButtonId id, final Currency currency) {
		return createConditionalButton(id, Text.PAY_AND_CREATE, currency.getEmojo(), true);
	}

	public static Button newPayAndSubmitButton() {
		return successButton(ButtonId.PAY_AND_SUBMIT, Text.PAY_AND_SUBMIT, Emojo.COIN);
	}

	public static Button newClearSubmissionButton() {
		return dangerButton(ButtonId.CLEAR_SUBMISSION, Text.CLEAR_SUBMISSION_BUTTON, Emojo.WASTEBASKET);
	}

	private static Button createConditionalButton(final ButtonId id, final String text, final Emojo emojo,
			boolean active) {
		Button base = active ? Button.success(id.name(), text) : Button.danger(id.name(), text).asDisabled();
		return base.withEmoji(emojo.getEmoji());
	}

	public static Button newGhostSubmissionButton() {
		return secondaryButton(ButtonId.GHOST_SUBMISSION, "ghost submission", Emojo.GHOST);
	}

//	public static Button newBuyCurrencyButton(final ShopProduct product, final Player targetPlayer) {
//		Button button = secondaryButton(product.getBuyButtonId(),
//				String.format(Buttons.BUY_X_TEMPLATE, product.getProductCount()), product.getProduct().getEmojo());
//		return product.canUserAfford(targetPlayer) ? button : button.asDisabled();
//	}

	public static Button newSummonCreateRiddleMenuButton() {
		return secondaryButton(ButtonId.SUMMON_CREATE_GAME, Text.SUMMON_CREATE_GAME_BUTTON, Emojo.GAME_DIE);
	}

	public static Button newDebugButton(String string) {
		return Button.secondary(string, string);
	}

	public static Button newRatingButton(RiddleRating rating) {
		switch (rating) {
		case MINUS:
			return dangerButton(ButtonId.valueOf(rating.name()), rating.toString(), null);

		case PLUS:
		case PLUS_PLUS:
			return successButton(ButtonId.valueOf(rating.name()), rating.toString(), null);
		}
		return null;
	}

}
