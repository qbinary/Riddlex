package dev.qbin.riddlex.bot.ui;

import java.util.List;

import dev.qbin.riddlex.model.guild.Guild;
import dev.qbin.riddlex.model.player.Player;

public class ModelStringifier {
	private ModelStringifier() {
	}

	public static String stringifyPlayerList(final List<Player> toStringify, long enumerationStart,
			long highlightedUserId) {
		StringBuilder sb = new StringBuilder();
		boolean enumerate = enumerationStart >= 0;
		long pos = enumerationStart;
		for (final Player o : toStringify) {
			if (pos != enumerationStart)
				sb.append('\n');
			pos++;
			if (enumerate)
				sb.append(String.format("%d. ", pos));
			if (o.getId() == highlightedUserId)
				sb.append("**");
			sb.append(o.toString());
			if (o.getId() == highlightedUserId)
				sb.append("**");
		}
		return sb.toString();
	}

	public static String stringifyServerList(final List<Guild> toStringify, long enumerationStart,
			long highlightedGuildId) {
		StringBuilder sb = new StringBuilder();
		boolean enumerate = enumerationStart >= 0;
		long pos = enumerationStart;
		for (final Guild o : toStringify) {
			if (pos != enumerationStart)
				sb.append('\n');
			pos++;
			if (enumerate)
				sb.append(String.format("%d. ", pos));
			if (o.getId() == highlightedGuildId)
				sb.append("**");
			sb.append(o.toString());
			if (o.getId() == highlightedGuildId)
				sb.append("**");
		}
		return sb.toString();
	}

}
