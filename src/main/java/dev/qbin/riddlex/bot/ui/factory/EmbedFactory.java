package dev.qbin.riddlex.bot.ui.factory;

import java.util.List;

import dev.qbin.riddlex.Constants;
import dev.qbin.riddlex.Constants.Embeds;
import dev.qbin.riddlex.Constants.HexColor;
import dev.qbin.riddlex.Constants.Resources;
import dev.qbin.riddlex.Constants.Text;
import dev.qbin.riddlex.Contributor;
import dev.qbin.riddlex.bot.ui.ModelStringifier;
import dev.qbin.riddlex.bot.ui.RiddlexError;
import dev.qbin.riddlex.bot.ui.TextEmbedContent;
import dev.qbin.riddlex.model.currency.Currency;
import dev.qbin.riddlex.model.game.Game;
import dev.qbin.riddlex.model.game.result.GameResult;
import dev.qbin.riddlex.model.game.result.GameResult.UserResult;
import dev.qbin.riddlex.model.guild.Guild;
import dev.qbin.riddlex.model.player.Player;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;

public class EmbedFactory {
	private EmbedFactory() {
	}

	private static EmbedBuilder decorateHint(final EmbedBuilder builder) {
		return builder;
	}

	private static EmbedBuilder textBox(final String title, final String description, final HexColor color) {
		return new EmbedBuilder().setTitle(title).setDescription(description).setColor(color.getColor());
	}

	public static MessageEmbed newSubmissionDuplicate() {
		EmbedBuilder embedBuilder = textBox(Text.ALREADY_SUBMITTED_TITLE, Text.ALREADY_SUBMITTED_DESCRIPTION,
				HexColor.ERROR);
		return embedBuilder.build();
	}

	public static MessageEmbed newNoGameHint() {
		return decorateHint(textBox(Text.NO_GAME_HINT_TITLE, Text.NO_GAME_HINT_DESCRIPTION, HexColor.HINT)).build();
	}

	public static MessageEmbed newCreateGameDisplay() {
		EmbedBuilder createGameDisplay = textBox(Text.CREATE_GAME_TITLE, Text.CREATE_GAME_DESCRIPTION, HexColor.CREATE);
		return createGameDisplay.build();
	}

	public static MessageEmbed newServerPouchDisplay(final String serverName, long ticketCount) {
		EmbedBuilder serverPouchDisplay = textBox(String.format(Text.SERVER_POUCH_TITLE_TEMPLATE, serverName), "",
				HexColor.POUCH);
		serverPouchDisplay.addField(FieldFactory.newCurrencyField(Currency.TICKET, ticketCount, true));
		return serverPouchDisplay.build();
	}

	public static MessageEmbed newGameAlreadyActiveHint() {
		// TODO more creative
		return decorateHint(
				textBox(Text.GAME_ALREADY_ACTIVE_TITLE, Text.GAME_ALREADY_ACTIVE_DESCRIPTION, HexColor.HINT)).build();
	}

	public static MessageEmbed newNotEnoughCurrencyHint(Currency currency) {
		// TODO more creative
		return decorateHint(textBox(String.format(Text.NOT_ENOUGH_TEMPLATE, currency.toString()), "", HexColor.HINT))
				.build();
	}

	public static MessageEmbed newErrorHint(RiddlexError error) {
		// TODO more creative
		return decorateHint(textBox(error.getErrorTitle(), error.getErrorDescription(), HexColor.ERROR)).build();
	}

	public static MessageEmbed newAlreadySubmittedHint() {
		// TODO more creative
		return textBox(Text.ALREADY_SUBMITTED_TITLE, Text.ALREADY_SUBMITTED_DESCRIPTION, HexColor.HINT).build();
	}

	public static MessageEmbed newPreparedSubmissionEmbed(String submission) {
		// TODO more creative
		return textBox(Text.PREPARED_SUBMISSION_TITLE, submission, HexColor.QUESTION).build();
	}

	public static MessageEmbed newEmbedOutdatedEmbed() {
		// TODO more creative
		return textBox(Text.EMBED_OUTDATED_TITLE, Text.EMBED_OUTDATED_DESCRIPTION, HexColor.HINT).build();
	}

	public static MessageEmbed newPreparedSubmissionClearedEmbed() {
		// TODO more creative
		return textBox(Text.PREPARED_SUBMISSION_CLEARED_TITLE, Text.PREPARED_SUBMISSION_CLEARED_DESCRIPTION,
				HexColor.HINT).build();
	}

	public static MessageEmbed newLoadingEmbed() {
		return new EmbedBuilder().setTitle(Constants.LOADING_TEXT).build();
	}

	public static MessageEmbed newExceptionEmbed(final Exception x) {
		EmbedBuilder builder = new EmbedBuilder();
		builder.setImage("https://picandocodigo.net/wp-content/uploads/2008/05/guru_meditation.gif");

		builder.addField("Exception", ":firecracker: An exception occured!", false);
		builder.addField("`" + x.getClass().getSimpleName() + "`", "`" + x.getMessage() + "`", false);

		for (int i = 0; i < 5; i++) {
			StackTraceElement ste = x.getStackTrace()[i];
			builder.addField("", "`" + ste.toString() + "`", false);
		}

		return builder.build();
	}

	public static MessageEmbed newGameCreatedEmbed() {
		return textBox(Text.GAME_CREATED_SUCCESSFULLY, "", HexColor.SUCCESS).build();
	}

	public static MessageEmbed newRiddleEmbed(final String riddle, final String solution, final String authorName) {
		EmbedBuilder builder = textBox(Embeds.RIDDLE_TITLE, riddle, HexColor.STATUS);

		builder.setFooter(authorName);

		// Add solution field if given
		if (solution != null) {
			builder.addField(FieldFactory.newSolutionField(solution, false));
			builder.setColor(HexColor.CLOSED_GAME.getColor());
		}

		return builder.build();
	}

	public static MessageEmbed newAlreadyClosedHint() {
		return decorateHint(
				textBox(Embeds.GAME_ALREADY_CLOSED_TITLE, Embeds.GAME_ALREADY_CLOSED_DESCRIPTION, HexColor.HINT))
				.build();
	}

	public static MessageEmbed newGameClosedEmbed() {
		// TODO Auto-generated method stub
		return null;
	}

	public static MessageEmbed newNotEnoughResponsesEmbed(long currentCount, long targetCount) {
		return textBox(Embeds.NOT_ENOUGH_RESPONSES_TITLE,
				String.format(Embeds.NOT_ENOUGH_RESPONSES_DESCRIPTION_TEMPLATE, targetCount, currentCount),
				HexColor.HINT).build();
	}

	public static MessageEmbed newGameClosedTitleEmbed() {
		return textBox(Embeds.GAME_CLOSED_TITLE, "", HexColor.SUCCESS).build();
	}

	public static MessageEmbed newNotEnoughTimePassedEmbed() {
		return textBox(Embeds.NOT_ENOUGH_TIME_PASSED_TITLE, Embeds.NOT_ENOUGH_TIME_PASSED_DESCRIPTION, HexColor.HINT)
				.build();
	}

	public static MessageEmbed newRaterHintEmbed() {
		return textBox(Embeds.RATER_HINT_TITLE, Embeds.RATER_HINT_DESCRIPTION, HexColor.STATUS).build();
	}

	public static MessageEmbed newLocalLeaderboardEmbed(final List<Player> entries, long enumerationStart,
			long highlightedUserId) {
		EmbedBuilder embed = textBox(Embeds.LOCAL_LEADERBOARD_TITLE,
				ModelStringifier.stringifyPlayerList(entries, enumerationStart, highlightedUserId),
				HexColor.LEADEBOARD);
		return embed.build();
	}

	public static MessageEmbed newGlobalLeaderboardEmbed(final List<Player> entries, long enumerationStart,
			long highlightedUserId) {
		EmbedBuilder embed = textBox(Embeds.GLOBAL_LEADERBOARD_TITLE,
				ModelStringifier.stringifyPlayerList(entries, enumerationStart, highlightedUserId),
				HexColor.LEADEBOARD);
		return embed.build();
	}

	public static MessageEmbed newFederatedLeaderboardEmbed(final List<Guild> entries, long enumerationStart,
			long highlightedUserId) {
		EmbedBuilder embed = textBox(Embeds.FEDERATED_LEADERBOARD_TITLE,
				ModelStringifier.stringifyServerList(entries, enumerationStart, highlightedUserId),
				HexColor.LEADEBOARD);
		return embed.build();
	}

	public static MessageEmbed newVersionDisplay() {
		EmbedBuilder embed = textBox(null, String.format("`%s`", Constants.FOOTPRINT), HexColor.HINT);
		return embed.build();
	}

	public static MessageEmbed newJoinHelpEmbed() {
		EmbedBuilder embed = textBox("Hello there! Thank you for using Riddlex!",
				"To setup Riddlex on your server you need to create a textchannel exclusivley for Riddlex and execute `/here` in it.",
				HexColor.SUCCESS);
		return embed.build();
	}

	public static MessageEmbed newFromContent(final TextEmbedContent content) {
		return textBox(content.getTitle(), content.getDescription(), content.getColor()).build();
	}

	public static MessageEmbed newClosedRiddleEmbed(final Game game) {
		EmbedBuilder embed = textBox(Embeds.RIDDLE_TITLE, game.getRiddle(), HexColor.STATUS);
		embed.addField(FieldFactory.newSolutionField(game.getSolution(), false));
		embed.setFooter(game.getHash());
		return embed.build();
	}

	public static MessageEmbed newGameResultEmbed(GameResult result, UserResult userResult) {
		Game game = result.getGame();

		EmbedBuilder builder = new EmbedBuilder();
		builder.setTitle(Embeds.GAME_RESULT_TITLE);
		builder.setDescription(game.getRiddle());
		builder.setFooter(game.getHash());

		builder.setColor((userResult.isCorrect() ? HexColor.SUCCESS.getColor() : HexColor.ERROR.getColor()));

		builder.addField(FieldFactory.newSolutionField(game.getSolution(), true));

		if (userResult.isCorrect()) {
			builder.setThumbnail(Resources.REWARD_IMAGE);
			builder.addField(FieldFactory.newRewardField(userResult.getReward(), Currency.PUZZLE, false));
		} else {
			builder.addField(FieldFactory.newSubmissionField(userResult.getSubmission(), false));
		}

		return builder.build();
	}

	public static MessageEmbed newRewardEmbed(int reward, final Currency currency) {
		EmbedBuilder builder = textBox(Embeds.REWARD_TITLE,
				String.format(Embeds.REWARD_DESCRIPTION_TEMPLATE, currency.printCount(reward, false)), HexColor.REWARD);
		builder.setThumbnail(Resources.REWARD_IMAGE);
		return builder.build();
	}

	public static MessageEmbed newInfoEmbed() {
		EmbedBuilder embed = new EmbedBuilder();
		embed.setTitle(Embeds.INFO_TITLE, Resources.GIT_REPO);
		embed.setThumbnail(Resources.FULL_AVATAR_IMAGE);
		embed.setColor(HexColor.HINT.getColor());

		for (final Contributor contrib : Contributor.values()) {
			embed.addField(FieldFactory.newContributorField(contrib, true));
		}

		embed.setFooter(String.format("Version: %s | Debug build: %s", Constants.FOOTPRINT,
				(Constants.DEBUG_BUILD ? "yes" : "no")));

		return embed.build();
	}

	public static MessageEmbed newError(String failMessage) {
		return textBox(Embeds.ERROR_TITLE, failMessage, HexColor.ERROR).build();
	}
}