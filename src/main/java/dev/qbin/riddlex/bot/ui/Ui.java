package dev.qbin.riddlex.bot.ui;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.command.GenericCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.components.ActionRow;
import net.dv8tion.jda.api.interactions.components.ItemComponent;
import net.dv8tion.jda.api.requests.restaction.interactions.ReplyCallbackAction;

public class Ui {
	private Collection<MessageEmbed> embeds;
	private Collection<ActionRow> actionRows;

	public Ui(final Collection<MessageEmbed> embeds, final List<ActionRow> list) {
		initialize(embeds, list);
	}

	public Ui(final MessageEmbed embed) {
		this.initialize(Arrays.asList(embed), Arrays.asList());
	}

	public Ui(final MessageEmbed... embeds) {
		this.initialize(Arrays.asList(embeds), Arrays.asList());
	}

	public Ui(MessageEmbed embeds, ItemComponent... closeGameButton) {
		if (closeGameButton.length == 0)
			this.initialize(Arrays.asList(embeds), Arrays.asList());
		else
			this.initialize(Arrays.asList(embeds), Arrays.asList(ActionRow.of(closeGameButton)));
	}

	public Ui(ItemComponent component, MessageEmbed... embeds) {
		this.initialize(Arrays.asList(embeds), Arrays.asList(ActionRow.of(component)));
	}

	private void initialize(final Collection<MessageEmbed> embeds, final Collection<ActionRow> list) {
		this.embeds = embeds;
		this.actionRows = list;
	}

	public Collection<ActionRow> getActionRows() {
		return actionRows;
	}

	public Collection<MessageEmbed> getEmbeds() {
		return embeds;
	}

	public ReplyCallbackAction reply(final GenericCommandInteractionEvent interaction) {
		return interaction.replyEmbeds(embeds).addActionRows(actionRows);
	}
}
