package dev.qbin.riddlex.bot.ui.factory;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import dev.qbin.riddlex.Constants.Resources;
import dev.qbin.riddlex.bot.ui.ButtonId;
import dev.qbin.riddlex.bot.ui.RiddlexError;
import dev.qbin.riddlex.bot.ui.TextEmbedContent;
import dev.qbin.riddlex.bot.ui.Ui;
import dev.qbin.riddlex.model.currency.Currency;
import dev.qbin.riddlex.model.game.Game;
import dev.qbin.riddlex.model.game.result.GameResult.UserResult;
import dev.qbin.riddlex.model.guild.Guild;
import dev.qbin.riddlex.model.player.Player;
import dev.qbin.riddlex.model.rating.RiddleRating;
import dev.qbin.riddlex.model.scoreboard.Scoreboard;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.emoji.Emoji;
import net.dv8tion.jda.api.interactions.components.ActionRow;
import net.dv8tion.jda.api.interactions.components.buttons.Button;

public class UiFactory {

	public static Ui buildNoGameMenu() {
		MessageEmbed noGameHint = EmbedFactory.newNoGameHint();
		Button summonCreateGameButton = ButtonFactory.newSummonCreateRiddleMenuButton();
		return new Ui(noGameHint, summonCreateGameButton);
	}

	public static Ui buildCreateGameMenu(final String guildName, long ticketCount) {
		MessageEmbed serverPouchDisplay = EmbedFactory.newServerPouchDisplay(guildName, ticketCount);
		MessageEmbed createGameDisplay = EmbedFactory.newCreateGameDisplay();

		Button createGameButton = ButtonFactory.newPayAndCreateButton(ButtonId.PAY_AND_CREATE_GAME, Currency.TICKET);

		return new Ui(createGameButton, serverPouchDisplay, createGameDisplay);
	}

	public static Ui buildGameAlreadyActivePlane() {
		MessageEmbed gameAlreadyActiveHint = EmbedFactory.newGameAlreadyActiveHint();
		return new Ui(gameAlreadyActiveHint);
	}

	public static Ui buildNotEnoughCurrencyPlane(final String guildName, final Currency currency,
			final long currencyCount) {
		MessageEmbed serverPouchDisplay = EmbedFactory.newServerPouchDisplay(guildName, currencyCount);
		MessageEmbed notEnoughCurrencyHint = EmbedFactory.newNotEnoughCurrencyHint(currency);
		return new Ui(serverPouchDisplay, notEnoughCurrencyHint);
	}

	public static Ui buildErrorPlane(final RiddlexError aiError) {
		MessageEmbed errorHint = EmbedFactory.newErrorHint(aiError);
		return new Ui(errorHint);
	}

	public static Ui buildPrepareSubmissionSuccessfullMenu(String submission) {
		MessageEmbed embed = EmbedFactory.newPreparedSubmissionEmbed(submission);

		Button submit = ButtonFactory.newPayAndSubmitButton();
		Button clear = ButtonFactory.newClearSubmissionButton();

		return new Ui(Arrays.asList(embed), Arrays.asList(ActionRow.of(submit), ActionRow.of(clear)));
	}

	public static Ui buildEmbedOutdatedHint() {
		MessageEmbed embedOutdatedHint = EmbedFactory.newEmbedOutdatedEmbed();
		return new Ui(embedOutdatedHint);
	}

	public static Ui buildPreparedSubmissionClearedHint() {
		MessageEmbed preparedSubmissionClearedHint = EmbedFactory.newPreparedSubmissionClearedEmbed();
		return new Ui(preparedSubmissionClearedHint);
	}

	public static Ui newAlreadyClosedMenu() {
		MessageEmbed alreadyClosedHint = EmbedFactory.newAlreadyClosedHint();
		MessageEmbed newGameHint = EmbedFactory.newNoGameHint();
		Button createNewGame = ButtonFactory.newSummonCreateRiddleMenuButton();
		return new Ui(createNewGame, alreadyClosedHint, newGameHint);
	}

	public static Ui newNotEnoughResponsesHint(long currentCount, long targetCount) {
		return new Ui(EmbedFactory.newNotEnoughResponsesEmbed(currentCount, targetCount));
	}

	public static Ui newNotEnoughTimePassedHint() {
		return new Ui(EmbedFactory.newNotEnoughTimePassedEmbed());
	}

	public static Ui newLeaderboardsView(final Scoreboard<Player> localPlayers, final Scoreboard<Guild> servers,
			final Scoreboard<Player> globalPlayers, long userId, long guildId, int entriesPerLeaderboard) {

		int globalUserIndex = globalPlayers.getPositionOf(userId);
		int localUserIndex = localPlayers.getPositionOf(userId);

		int guildIndex = servers.getPositionOf(guildId);

		List<MessageEmbed> embeds = new LinkedList<>();

		List<Player> localRange = localPlayers.getArround(localUserIndex, entriesPerLeaderboard);
		if (localRange.size() > 0) {
			int localFirstPosition = localPlayers.getPositionOf(localRange.get(0).getId());
			MessageEmbed localLeaderboard = EmbedFactory.newLocalLeaderboardEmbed(localRange, localFirstPosition,
					userId);
			embeds.add(localLeaderboard);
		}

		List<Player> globalRange = globalPlayers.getArround(globalUserIndex, entriesPerLeaderboard);
		if (globalRange.size() > 0) {
			int globalFirstPosition = globalPlayers.getPositionOf(globalRange.get(0).getId());
			MessageEmbed globalLeaderboard = EmbedFactory.newGlobalLeaderboardEmbed(globalRange, globalFirstPosition,
					userId);
			embeds.add(globalLeaderboard);
		}

		List<Guild> federatedRange = servers.getArround(guildIndex, entriesPerLeaderboard);
		if (federatedRange.size() > 0) {
			int federatedFirstPosition = servers.getPositionOf(federatedRange.get(0).getId());
			MessageEmbed federatedLeaderboard = EmbedFactory.newFederatedLeaderboardEmbed(federatedRange,
					federatedFirstPosition, guildId);
			embeds.add(federatedLeaderboard);
		}

		return new Ui(embeds, Arrays.asList());
	}

	public static Ui newPlainRiddleGameDisplay(Game plainRiddleGame) {
		return new Ui(EmbedFactory.newRiddleEmbed(plainRiddleGame.getRiddle(),
				(plainRiddleGame.isClosed() ? plainRiddleGame.getSolution() : null), plainRiddleGame.getCreator()));
	}

	public static Ui newGameResult(Game game, UserResult result) {
		Button bad = ButtonFactory.newRatingButton(RiddleRating.MINUS);
		Button decent = ButtonFactory.newRatingButton(RiddleRating.PLUS);
		Button excellent = ButtonFactory.newRatingButton(RiddleRating.PLUS_PLUS);

		return new Ui(Arrays.asList(EmbedFactory.newGameResultEmbed(game.getGameResult(), result),
				EmbedFactory.newRaterHintEmbed()), Arrays.asList(ActionRow.of(bad, decent, excellent)));
	}

	public static Ui newInfoUi() {
		MessageEmbed infoEmbed = EmbedFactory.newInfoEmbed();
		Button gitRepo = Button.link(Resources.GIT_REPO, "Git").withEmoji(Emoji.fromUnicode("⌨️"));
		return new Ui(infoEmbed, gitRepo);
	}

	public static Ui newHelpUi() {
		MessageEmbed help = EmbedFactory.newFromContent(TextEmbedContent.HELP_UI_START);
		Button player = Button.primary(ButtonId.HELP_PLAYER.name(), "Player Help").withEmoji(Emoji.fromUnicode("🎮"));
		Button admin = Button.primary(ButtonId.HELP_ADMIN.name(), "Admin Help").withEmoji(Emoji.fromUnicode("🔧"));
		return new Ui(help, player, admin);
	}

}
