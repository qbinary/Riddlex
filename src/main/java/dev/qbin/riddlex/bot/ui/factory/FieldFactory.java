package dev.qbin.riddlex.bot.ui.factory;

import dev.qbin.riddlex.Constants.Fields;
import dev.qbin.riddlex.Contributor;
import dev.qbin.riddlex.model.currency.Currency;
import net.dv8tion.jda.api.entities.MessageEmbed.Field;

public class FieldFactory {
	private FieldFactory() {
	}

	public static Field newRiddleField(final String riddle, boolean inline) {
		return new Field(Fields.RIDDLE_NAME, riddle, inline);
	}

	public static Field newSolutionField(final String solution, boolean inline) {
		return new Field(Fields.SOLUTION_NAME, solution, inline);
	}

	public static Field newResultsField(final String results, boolean inline) {
		return new Field(Fields.SOLUTION_NAME, results, inline);
	}

	public static Field newCurrencyField(final Currency currency, long count, boolean inline) {
		return new Field(currency.printCount(count, false), currency.getPlural(), inline);
	}

//	public static Field newCorrectSubmissionField(final ClosedGameSubmission cgs, boolean inline) {
//		return new Field(String.format(Fields.CORRECT_SUBMISSION_TITLE_TEMPLATE, cgs.getPlacement(), cgs.getUsername()),
//				String.format(Fields.CORRECT_SUBMISSION_DESCRIPTION_TEMPLATE, cgs.getReward()), inline);
//	}
//
//	public static Field newIncorrectSubmissionField(final ClosedGameSubmission cgs) {
//		return new Field(String.format(Fields.INCORRECT_SUBMISSION_TITLE_TEMPLATE, cgs.getUsername()),
//				String.format(Fields.INCORRECT_SUBMISSION_DESCRIPTION_TEMPLATE, cgs.getSubmission()), true);
//	}

	public static Field newPricetagField(final Currency productCurrency, long productCount,
			final Currency paymentCurrency, long price, boolean inline) {
		return new Field(productCurrency.printCount(productCount, false), paymentCurrency.printCount(price, true),
				inline);
	}

	public static Field newSubmissionField(final String submission, boolean inline) {
		return new Field(Fields.SUBMISSION_NAME, submission, inline);
	}

	public static Field newRewardField(int reward, final Currency currency, boolean inline) {
		return new Field(Fields.REWARD_NAME, currency.printCount(reward, false), inline);
	}

	public static Field newVersionField(String footprint, boolean debugBuild, boolean inline) {
		return new Field(String.format(Fields.VERSION_NAME_TEMPLATE, (debugBuild ? ":wrench:" : ":gear:")), footprint,
				inline);
	}

	public static Field newContributorField(Contributor contrib, boolean inline) {
		return new Field(String.format("%s %s", contrib.getName(), contrib.getType().getEmoji()),
				contrib.getDescription(), inline);
	}
}
