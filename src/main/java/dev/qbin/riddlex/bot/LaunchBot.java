package dev.qbin.riddlex.bot;

import java.io.IOException;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.Scanner;

import javax.security.auth.login.LoginException;

import dev.qbin.gaiji.GaijiService;
import dev.qbin.gaiji.openai.api.OpenAiAPI;
import dev.qbin.riddlex.Constants;
import dev.qbin.riddlex.ai.AiInterface;
import dev.qbin.riddlex.bot.commands.RiddlexCommand;
import dev.qbin.riddlex.bot.tools.Wordlist;
import dev.qbin.riddlex.util.LookupProvider;
import dev.qbin.riddlex.util.config.Config;
import dev.qbin.riddlex.util.log.Log;
import dev.qbin.riddlex.util.persistence.PersistenceException;
import dev.qbin.riddlex.util.persistence.maria.MariaPersistence;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.requests.GatewayIntent;

public class LaunchBot {
	public static void main(String[] args) throws LoginException, IOException, InterruptedException, SQLException,
			PersistenceException, InstantiationException, IllegalAccessException {

		Log.setVerbose(!Constants.RELEASE_BUILD);

		// Load configuration
		Config config = new Config(Paths.get(Constants.CONFIG_FILE_PATH));

		// Initialize AI
		GaijiService.getInstance().setApiToken(config.getOpenAiApiToken());

		// Build JDA
		JDABuilder jdaBuilder = JDABuilder.createDefault(config.getDiscordApiToken());
		jdaBuilder.enableIntents(GatewayIntent.MESSAGE_CONTENT);

		JDA jda = jdaBuilder.build();
		jda.awaitReady();

		// Upsert commands
		LinkedList<CommandData> commands = new LinkedList<>();
		for (final RiddlexCommand c : RiddlexCommand.values()) {
			CommandData current = c.buildCommandData();
			commands.add(current);
			jda.upsertCommand(current).complete();
		}
		jda.updateCommands().addCommands(commands).complete();

		jda.getPresence().setActivity(Activity.watching("you struggle"));

		// Initialize Context
		LookupProvider<String, String> riddleProvider;
		if (Constants.DEBUG_BUILD)
			riddleProvider = new LookupProvider<String, String>() {
				@Override
				public String provide(String key) {
					return "dummy riddle";
				}
			};
		else
			riddleProvider = new AiInterface(new OpenAiAPI(config.getOpenAiApiToken()));
		RiddlexContext context = new RiddlexContext(jda, new MariaPersistence(config.getMariaSpec(), jda),
				new Wordlist(), riddleProvider, config);
		context.getFeatureManager().loadFeatures(Constants.ACTIVE_FEATURES);
		context.getFeatureManager().enableDebugFeature();

		new Thread(new Runnable() {

			@Override
			public void run() {
				Scanner sc = new Scanner(System.in);
				sc.nextLine();
				sc.close();
				context.stop();
			}
		}).start();
	}
}
