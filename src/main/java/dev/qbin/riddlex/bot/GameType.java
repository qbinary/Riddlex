package dev.qbin.riddlex.bot;

public enum GameType {
	DAILY_GAME("Daily Riddle", true, true), WEEKLY_GAME("Weekly Riddle", true, true),
	GUILD_GAME("Custom Game", false, false);

	private String gameName;
	private boolean federated;
	private boolean periodic;

	private GameType(final String gameName, boolean federated, boolean periodic) {
		this.gameName = gameName;
		this.federated = federated;
		this.periodic = periodic;
	}

	public String getGameName() {
		return gameName;
	}

	public boolean isFederated() {
		return federated;
	}

	public boolean isPeriodic() {
		return periodic;
	}
}
