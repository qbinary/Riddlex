package dev.qbin.riddlex.bot.tools;

import java.util.Arrays;

public class DiscordTextTool {
	private DiscordTextTool() {
	}

	public static final String formatException(Exception e, boolean stacktrace) {
		StringBuilder stackTraceText = new StringBuilder();
		if (stacktrace) {
			Arrays.asList(e.getStackTrace()).forEach(s -> {
				if (stackTraceText.length() < 100)
					stackTraceText.append(String.format("> `%s`\n", s.toString()));
			});
		}

		return String.format(":x: Error occured:\n`%s`\n%s", e.getClass().getSimpleName(), stackTraceText.toString());
	}

	public static final String codeInline(final String content) {
		return String.format("`%s`", content);
	}

	public static final String codeBlock(final String highlighting, final String content) {
		return String.format("```%s\n%s\n```", content);

	}
}
