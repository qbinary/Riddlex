package dev.qbin.riddlex.bot.tools;

public class Formatter {
	private Formatter() {
	}

	public static String format(long number, int digitCount) {
		StringBuilder result = new StringBuilder();

		String numberString = Long.toString(number);
		for (int i = 0; i < digitCount - numberString.length(); i++)
			result.append('0');
		result.append(numberString);

		return result.toString();
	}

	public static String getCropped(final String subject, int maxChars) {
		if (subject.length() > maxChars)
			return subject.substring(maxChars - 1) + '…';
		return subject;
	}
}
