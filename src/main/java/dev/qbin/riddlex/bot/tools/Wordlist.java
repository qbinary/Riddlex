package dev.qbin.riddlex.bot.tools;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;

import dev.qbin.riddlex.util.Provider;

public class Wordlist implements Provider<String> {

	private Random rand = new Random();
	private String[] wordlist;

	public Wordlist() {
		try {
			wordlist = new String(Files.readAllBytes(Paths.get("english_nouns.txt"))).split("\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getRandomWord() {
		rand.setSeed(System.currentTimeMillis());
		return wordlist[rand.nextInt(wordlist.length - 1)];
	}

	public boolean containsWord(final String check) {
		for (final String i : wordlist)
			if (i.equals(check))
				return true;
		return false;
	}

	@Override
	public String provide() {
		return getRandomWord();
	}
}
