package dev.qbin.riddlex.bot.tools;

import dev.qbin.riddlex.util.Provider;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.User;

public class UsernameProvider implements Provider<String> {

	private JDA jda;
	private long userId;

	public static UsernameProvider createForUser(User user) {
		return new UsernameProvider(user.getJDA(), user.getIdLong());
	}

	public UsernameProvider(final JDA jda, long userId) {
		this.jda = jda;
		this.userId = userId;
	}

	@Override
	public String provide() {
		User user = jda.getUserById(userId);
		if(user == null)
			user = jda.retrieveUserById(userId).complete();
		return user.getName();
	}
}
