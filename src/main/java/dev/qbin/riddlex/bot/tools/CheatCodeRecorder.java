package dev.qbin.riddlex.bot.tools;

import java.util.LinkedList;

public class CheatCodeRecorder {

	public enum Input {
		UP, DOWN, LEFT, RIGHT, A, B
	}

	private Input[] CHEAT_CODE = new Input[] { Input.UP, Input.UP, Input.DOWN, Input.DOWN, Input.LEFT, Input.RIGHT,
			Input.LEFT, Input.RIGHT, Input.B, Input.A };

	private LinkedList<Input> inputs = new LinkedList<>();

	public boolean input(final Input input) {
		inputs.add(input);
		return CHEAT_CODE.length - inputs.size() <= 0;
	}

	public boolean check() {
		boolean correct = true;
		if (inputs.size() == CHEAT_CODE.length)
			for (int i = 0; i < CHEAT_CODE.length; i++) {
				if (CHEAT_CODE[i] != inputs.get(i)) {
					correct = false;
					break;
				}
			}
		else 
			correct = false;
		inputs.clear();
		return correct;
	}

}
