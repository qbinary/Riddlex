package dev.qbin.riddlex.bot.tools;

import dev.qbin.riddlex.util.Provider;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;

public class GuildNameProvider implements Provider<String> {

	private final JDA JDA;
	private long guildId;

	public GuildNameProvider(final JDA JDA, long guildId) {
		this.JDA = JDA;
		this.guildId = guildId;
	}

	@Override
	public String provide() {
		Guild guild = JDA.getGuildById(guildId);
		if (guild == null)
			return null;
		return guild.getName();
	}

}
