package dev.qbin.riddlex.bot.tools;

import net.dv8tion.jda.api.entities.emoji.Emoji;

public enum Emojo {
	SMALL_ORANGE_DIAMOND("small_orange_diamond", "U+1F538"), COIN("coin", "U+1FA99"), TICKETS("tickets", "U+1F39F"),
	GHOST("ghost", "U+1F47B"), GAME_DIE("game_die", "U+1F3B2"), WASTEBASKET("wastebasket", "U+1F5D1"),
	JIGSAW("jigsaw", "U+1F9E9"), HEAVY_MINUS_SIGN("heavy_minus_sign", "U+2796"),
	WHITE_MEDIUM_SQUARE("white_medium_square", "U+25FB"), HEAVY_PLUS_SIGN("heavy_plus_sign", "U+2795");

	private String markdown;
	private String unicode;

	Emojo(final String markdown, final String unicode) {
		this.markdown = markdown;
		this.unicode = unicode;
	}

	public String getEmbedMarkdown() {
		return String.format(":%s:", markdown);
	}

	public String getMarkdown() {
		return markdown;
	}

	public String getUnicode() {
		return unicode;
	}

	public Emoji getEmoji() {
		return Emoji.fromUnicode(unicode);
	}
}
