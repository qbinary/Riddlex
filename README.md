# Riddlex - The AI Riddle Game for Discord 
### What is it?
It is an AI based Discord game which **generates** riddles the users can solve. You can compete against your friends and raise in your servers leadeboard. 
There is also a global server and user leadeboard. 

### How to test/contribute
* You can play arround with Riddlex on my demo Discord  
:link: [Join here (Discord server invite)](https://discord.gg/TAmEfDvR85)
* You can contact me if you want to add it onto your own server
* Host it on your own

#### Features
* :game_die: Creative AI generated riddles
* :sparkles: Beautiful user interface
* :pencil2: A rating system to improve the models accuracy

### Who created this cool profile picture?
It was [OpenAi's DALL·E 2](https://openai.com/dall-e-2/)

### How does it work?
1. Riddlex picks a random word from a text file. This is the **solution** to the riddle.
2. This random word is then sent to GPT3 which generates a riddle for the given solution.
3. The riddle is then posted to the users on Discord.
4. The users can submit solutions which get checked by a simple string compare to determine if they are correct.
5. If the submission is correct the user will be rewarded.

### Which improvements are planed?
Okay thats pretty cool so far but in the future I want to implement some more things:
1. Instead of rating the submissions by a simple string compare they will run through a classification model which rates the quality of the solution. That would allow to grant rewards for multiple correct solutions in case of ambiguous riddles. 
2. Improve the riddle generation model by fine-tuning it with good riddle examples it generated itself.